package com.example.ibdb.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.Notification;
import com.example.ibdb.domain.User;

@Repository
public interface notificationRepository extends JpaRepository<Notification, Long> {
	
	@Query("SELECT n from Notification n where n.user=:user order by timestamp desc")
	List<Notification> findAllByUser(Pageable pageable,@Param("user") User user);
	
	@Query("SELECT count(n) from Notification n where n.user=:user and n.read=false")
	Long getUnreadNotificationCount(@Param("user") User user);
	
	@Query("SELECT count(n) from Notification n where n.user=:user")
	Long getNotificationCount(@Param("user") User user);
	
	@Modifying
	@Query("UPDATE Notification n set n.read=true where n.user=:user and n.read=false")
	void markNotificationsAsSeen(@Param("user") User user);

	void deleteByUser(User user);

	void deleteByBook(Book book);
}
