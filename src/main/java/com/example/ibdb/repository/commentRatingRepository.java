package com.example.ibdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.CommentRating;

@Repository
public interface commentRatingRepository extends JpaRepository<CommentRating, Long> {

	void deleteById(Long id);

	CommentRating findOneById(Long id);


}
