package com.example.ibdb.repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.Rating;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
public interface bookRepository extends JpaRepository<Book, Long> {
    void deleteByIsbn(String isbn);
    
	@Query("SELECT b "
		 + "from Book b "
		 + "where (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%')))")
    List<Book> findAllBooks(Pageable pageable, @Param("searchPhrase") String searchPhrase);

	@Query("SELECT count(b) "
		 + "from Book b "
		 + "where (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) ")
    Long findAllBooksCount(@Param("searchPhrase") String searchPhrase);
	
    @Query("SELECT count (*) "
    	 + "from Book b "
    	 + "where b.isbn in (SELECT b.isbn "
    	 + "                 from Book b "
    	 + "                 left join b.bookCopies bc on bc.available='true' "
    	 + "                 left join b.ratings r on r.user.birthday>=:startAge "
    	 + "                                      and r.user.birthday <=:endAge "
    	 + "                 where b.isbn in (select b.isbn "
    	 + "                                 from Book b "
    	 + "                                 where (:searchPhrase is null "
    	 + "                                    or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                                    or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                                    or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                                    or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                                    or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%')))) "
    	 + "                group by b " 
    	 + "                having (:minimumRating is null "
    	 + "                    or avg(r.rating) is null "
    	 + "                    or avg(r.rating) >= :minimumRating))")
    Long findBookCount(@Param("startAge") LocalDate startAge,
                       @Param("endAge") LocalDate endAge, 
                       @Param("searchPhrase") String searchPhrase,
                       @Param("minimumRating") Double minimumRating);

    @Query("SELECT new Book(b, avg(r.rating)) from Book b left join b.ratings r where b.isbn=:isbn group by b")
    Book findOneByIsbn(@Param("isbn") String isbn);
    
    @Query("SELECT new Book(b,count(bc))"
    	+ " from Book b "
    	+ " left outer join b.bookCopies bc on bc.available='true' "
    	+ " where b.isbn in :isbns "
    	+ " group by b.isbn")
    List<Book> findBookRatings(@Param("isbns") List<String> isbns);

    @Query("SELECT new Book(b, avg(r.rating), count(bc)) "
     	 + "from Book b "
    	 + "left join b.bookCopies bc on bc.available='true' "
    	 + "left join b.ratings r on r.user.birthday>=:startAge "
    	 + "                     and r.user.birthday <=:endAge "
    	 + "where b.isbn in (select  b.isbn "
    	 + "                 from Book b "
    	 + "                 where (:searchPhrase is null "
    	 + "                    or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                    or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                    or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                    or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
    	 + "                    or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%')))) " 
    	 + "group by b "
         + "having (:minimumRating is null or avg(r.rating) is null or avg(r.rating) >= :minimumRating)")
    List<Book> findRatings(Pageable pageable, 
    		               @Param("startAge") LocalDate startAge,
                           @Param("endAge") LocalDate endAge, 
                           @Param("searchPhrase") String searchPhrase,
                           @Param("minimumRating") Double minimumRating);

    @Query("SELECT new Book(b, avg(r.rating)) " 
         + "from Book b "
         + "join b.ratings r "
         + "join r.user u "
         + "where u.birthday>=:startAge "
         + "  and u.birthday<=:endAge "
         + "group by b "
         + "order by avg(r.rating)+0 desc, b.bookTitle asc")
    List<Book> findTopRatings(Pageable pageable,
    		                  @Param("startAge") LocalDate startAge, 
    		                  @Param("endAge") LocalDate endAge);

    @Query("SELECT new Book(b, avg(r.rating)) from Book b left join b.ratings r where b.isbn=:isbn group by b")
    Book findBookWithRating(@Param("isbn") String isbn);

    @Query("SELECT r "
    	 + "from Rating r "
    	 + "join r.book b "
    	 + "join r.user u "
    	 + "where u.userId=:userId " 
    	 + "  and (:searchPhrase is null "
    	 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%')))")
    List<Rating> findCurrentUserRatings(Pageable pageable, 
    		                            @Param("userId") Long userId,
                                        @Param("searchPhrase") String searchPhrase);
    
    @Query("select b "
       	 + "from Rating r "
       	 + "join r.user u "
       	 + "join r.book b "
       	 + "where u.userId=:userId "
       	 + "order by r.rating desc")
       List<Book> findTop10CurrentUserHighestRatedBooks(@Param("userId") Long userId);
       
       @Query("select new Book(b,avg(r.rating)) "
       	 + "from Book b "
       	 + "left join b.ratings r on r.user.birthday<=:startAge "
       	 + "                     and r.user.birthday >=:endAge "
       	 + "                     and (:filterPhrase is null) "
       	 + "where b.isbn in :bookIsbns group by b")
       List<Book> findBooksByIsbn (@Param("bookIsbns") Set<String> bookIsbns, Pageable pageable, 
       		@Param("startAge") LocalDate startAge, @Param("endAge") LocalDate endAge, 
       		@Param("filterPhrase") String filterPhrase);
       
       @Query("select count(b) "
       	 + "from Book b "
       	 + "where b.isbn in :bookIsbns")
       Long findBookCountByIsbn (@Param("bookIsbns") Set<String> bookIsbns);
       
       @Query("select u.userId "
       	 + "from Rating r "
       	 + "join r.user u "
       	 + "join r.book b"
       	 + " where b.isbn=:isbn "
       	 + "   and u.userId!=:userId "
       	 + "order by r.rating desc")
       List<Long> findTop10UsersWithHighestRatings (@Param("isbn") String isbn, @Param("userId") Long userId);
       
       @Query("select b.isbn "
       	 + "from Rating r "
       	 + "join r.user u "
       	 + "join r.book b "
       	 + "where b.isbn not in (select b.isbn "
       	 + "                     from Rating r "
       	 + "                     join r.user u "
       	 + "                     join r.book b "
       	 + "                     where u.userId=:currentUserId) "
       	 + "  and u.userId=:userId "
       	 + "order by r.rating desc")
       List<String> findTop3UserHighestRatedBooks(@Param("userId") Long userId, @Param("currentUserId") Long currentUserId);
}