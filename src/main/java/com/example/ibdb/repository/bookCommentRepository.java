package com.example.ibdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookComment;
import com.example.ibdb.domain.User;

import java.util.List;

public interface bookCommentRepository extends JpaRepository<BookComment, Long> {
    
	
	List<BookComment> findAllByBookAndParentCommentIsNull(Book book);

	BookComment findOneByCommentId(Long commentId);
	
	@Modifying
	@Query("UPDATE BookComment b set b.user=null where b.user=:user")
	void deleteUserComments(@Param("user") User user);

	void deleteByBook(Book book);

}
