package com.example.ibdb.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.LentBook;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.User;
import com.example.ibdb.model.BookLendingStatus;

@Repository
public interface lentBookRepository extends JpaRepository<LentBook, Long> {
	LentBook findOneById(Long id);

	void deleteByBookCopy(BookCopy bookCopy);

	void deleteByUser(User user);

	@Query("select lb "
		 + "from LentBook lb "
		 + "where lb.bookCopy=:bookCopy "
		 + "  and lb.toDate >= :today "
		 + "  and (lb.lendingStatus='BOOK_RESERVED' or lb.lendingStatus='ACTIVE')")
	List<LentBook> findAllByBookCopy(@Param("bookCopy") BookCopy bookCopy, @Param("today") LocalDate today);

	@Query("select lb "
		 + "from LentBook lb "
		 + "where lb.bookCopy=:bookCopy"
		 + "  and ((lb.fromDate between :fromDate and :toDate) "
		 + "   or (lb.toDate between :fromDate and :toDate) "
		 + "   or (:fromDate between lb.fromDate and lb.toDate) "
		 + "   or (:toDate between lb.fromDate and lb.toDate)) "
		 + "  and (lb.lendingStatus='BOOK_RESERVED' "
		 + "   or lb.lendingStatus='ACTIVE')")
	List<LentBook> findAllByBookCopyBetweenDates(@Param("bookCopy") BookCopy bookCopy,
			@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate);

	@Query("select distinct lb.lendingStatus from LentBook lb join lb.user u where u.userId=:userId")
	List<BookLendingStatus> findCurrentUserLendingStatuses(@Param("userId") Long userId);
    
    void deleteByLibrary(Library library);
	
	@Query("select count(lb) "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where u.userId=:userId "
		 + "  and (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) ")
	Long findCurrentUserLentBookCount(@Param("userId") Long userId, @Param("searchPhrase") String searchPhrase,
			@Param("lendingStatus") BookLendingStatus lendingStatus, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate,@Param("libraryId") Long libraryId);

	@Query("Select lb "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where u.userId=:userId "
		 + "  and (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "order by lb.timestamp desc")
	List<LentBook> findCurrentUserLentBooks(Pageable pageable, 
											@Param("userId") Long userId,
											@Param("searchPhrase") String searchPhrase, 
											@Param("lendingStatus") BookLendingStatus lendingStatus,
											@Param("fromDate") LocalDate fromDate, 
											@Param("toDate") LocalDate toDate,
											@Param("libraryId") Long libraryId);
	
	@Query("Select b "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where u.userId=:userId "
		 + "  and (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "group by b")
	List<Book> findCurrentUserUniqueLentBooks(Pageable pageable, 
											  @Param("userId") Long userId,
											  @Param("searchPhrase") String searchPhrase, 
										      @Param("lendingStatus") BookLendingStatus lendingStatus,
										      @Param("fromDate") LocalDate fromDate, 
											  @Param("toDate") LocalDate toDate,
											  @Param("libraryId") Long libraryId);
	
	@Query("select count(lb) "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:bookSearchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:bookSearchPhrase,'%'))) "
		 + "  and (:userSearchPhrase is null "
		 + "   or lower(u.email) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.firstName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.lastName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.alias) like lower(concat('%',:userSearchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate)"
		 + "  and (:libraryId is null or l.libraryId=:libraryId)")
	Long findLentBookCount(@Param("bookSearchPhrase") String bookSearchPhrase,
						   @Param("userSearchPhrase") String userSearchPhrase, 
						   @Param("lendingStatus") BookLendingStatus lendingStatus,
						   @Param("fromDate") LocalDate fromDate, 
						   @Param("toDate") LocalDate toDate,
						   @Param("libraryId") Long libraryId);

	@Query("Select lb "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:bookSearchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:bookSearchPhrase,'%'))) "
		 + "  and (:userSearchPhrase is null "
		 + "   or lower(u.email) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.firstName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.lastName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.alias) like lower(concat('%',:userSearchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId)")
	List<LentBook> findLentBooks(Pageable pageable, 
								 @Param("bookSearchPhrase") String bookSearchPhrase,
								 @Param("userSearchPhrase") String userSearchPhrase, 
								 @Param("lendingStatus") BookLendingStatus lendingStatus,
								 @Param("fromDate") LocalDate fromDate, 
								 @Param("toDate") LocalDate toDate,
								 @Param("libraryId") Long libraryId);
	
	@Query("Select u "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where (:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:bookSearchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:bookSearchPhrase,'%'))) "
		 + "  and (:userSearchPhrase is null "
		 + "   or lower(u.email) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.firstName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.lastName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.alias) like lower(concat('%',:userSearchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "group by u")
	List<User> findUniqueLentBookUsers(Pageable pageable,
									   @Param("bookSearchPhrase") String bookSearchPhrase,
									   @Param("userSearchPhrase") String userSearchPhrase, 
									   @Param("lendingStatus") BookLendingStatus lendingStatus,
									   @Param("fromDate") LocalDate fromDate,
									   @Param("toDate") LocalDate toDate,
									   @Param("libraryId") Long libraryId);
	
	@Query("Select b "
		 + "from LentBook lb "
		 + "join lb.user u "
		 + "join lb.bookCopy bc "
		 + "join bc.book b "
		 + "join bc.library l "
		 + "where(:lendingStatus is null or lb.lendingStatus=:lendingStatus) "
		 + "  and (:bookSearchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:bookSearchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:bookSearchPhrase,'%'))) "
		 + "  and (:userSearchPhrase is null "
		 + "   or lower(u.email) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.firstName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.lastName) like lower(concat('%',:userSearchPhrase,'%')) "
		 + "   or lower(u.alias) like lower(concat('%',:userSearchPhrase,'%'))) "
		 + "  and (:fromDate is null or lb.fromDate >= :fromDate) "
		 + "  and (:toDate is null or lb.toDate <= :toDate) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "group by b")
	List<Book> findUniqueLentBookBooks(Pageable pageable, 
									   @Param("bookSearchPhrase") String bookSearchPhrase,
									   @Param("userSearchPhrase") String userSearchPhrase, 
									   @Param("lendingStatus") BookLendingStatus lendingStatus,
									   @Param("fromDate") LocalDate fromDate, 
									   @Param("toDate") LocalDate toDate,
									   @Param("libraryId") Long libraryId);
}
