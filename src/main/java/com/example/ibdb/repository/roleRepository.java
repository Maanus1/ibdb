package com.example.ibdb.repository;

import com.example.ibdb.model.RoleName;
import com.example.ibdb.domain.Role;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface roleRepository extends JpaRepository<Role, Long> {
    Role findByName(RoleName roleName);
    
}