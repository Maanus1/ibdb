package com.example.ibdb.repository;

import com.example.ibdb.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface userRepository extends JpaRepository<User, Long> {
    @Query("SELECT u from User u where u.email=:email")
    User findOneUserByEmail(@Param("email") String email);

    User findOneByEmail(String email);

    @Query("Select u from User u where u.userId=:userId")
    User findOneByUserId(@Param("userId") Long userId);

    @Query("SELECT u "
    	 + "from User u "
    	 + "where :searchPhrase is null "
    	 + "   or lower(u.email) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(u.alias) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(u.firstName) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or lower(u.lastName) like lower(concat('%',:searchPhrase,'%'))")
    List<User> findAllUsers(Pageable pageable, @Param("searchPhrase") String searchPhrase);

    @Query("SELECT count(u) "
    	 + "from User u"
    	 + " where :searchPhrase is null "
    	 + "    or lower(u.email) like lower(concat('%',:searchPhrase,'%')) "
    	 + "    or lower(u.alias) like lower(concat('%',:searchPhrase,'%')) "
    	 + "    or lower(u.firstName) like lower(concat('%',:searchPhrase,'%')) "
    	 + "    or lower(u.lastName) like lower(concat('%',:searchPhrase,'%'))")
    Long findUserCount(@Param("searchPhrase") String searchPhrase);

	User findOneByAlias(String alias);
}

