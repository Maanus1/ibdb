package com.example.ibdb.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.Library;

@Repository
public interface bookCopyRepository extends JpaRepository<BookCopy, Long> {
	BookCopy findOneByBookId(Long bookId);

	@Query("SELECT bc "
		 + "from BookCopy bc "
		 + "join bc.library l "
		 + "where bc.book=:book "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "  and bc.available=true")
	List<BookCopy> findAllByBookForUser(@Param("book") Book book, @Param("libraryId") Long libraryId);

	List<BookCopy> findAllByBook(Pageable pageable, Book book);

	@Query("SELECT count(bc) "
		 + "from BookCopy bc "
		 + "join bc.library l "
		 + "where bc.book.isbn=:isbn "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "  and (:available is null or bc.available=:available)")
	Long findBookCopyCount(@Param("isbn") String isbn,@Param("libraryId") Long libraryId,
			@Param("available") Boolean available);

	List<BookCopy> findAllByBook(Book book);

	List<BookCopy> findAllByLibrary(Library library);

	List<BookCopy> findAllByLibrary(Pageable pageable, Library library);

	@Query("SELECT count(bc) from BookCopy bc join bc.library l where l.libraryId=:libraryId")
	Long findLibraryBookCopyCount(@Param("libraryId") Long libraryId);
	
	@Query("SELECT bc "
		 + "from BookCopy bc "
		 + "join bc.library l "
		 + "where bc.book=:book "
		 + "  and (:library is null or l=:library) "
		 + "  and (:available is null or bc.available=:available)")
	List<BookCopy> findAllByBookAndLibraryAndAvailable(Pageable pageable,@Param("book") Book book,@Param("library") Library library,
			@Param("available") Boolean available);

	BookCopy findOneByLibraryAndBookId(Library library, Long bookId);
}
