package com.example.ibdb.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookWaitListItem;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.User;
@Repository
public interface bookWaitListItemRepository extends JpaRepository<BookWaitListItem, Long> {

	BookWaitListItem findOneByLibraryAndBookAndUser(Library library, Book book, User user);
	
	
	
	@Query("Select new BookWaitListItem(b.book,b.library) "
			 + "from BookWaitListItem b "
			 + "left outer join b.library library " 
			 + "group by b.book,b.library")
	List<BookWaitListItem> findAllBooksAndLibraries();
	
	@Query("Select new BookWaitListItem(b,l,count(bw)) "
		 + "from BookWaitListItem bw "
		 + "inner join bw.book b "
		 + "left outer join bw.library l "
		 + "where (:library is null or l=:library) "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "group by b,l "
		 + "order by count(b) desc")
	List<BookWaitListItem> findAllByBookAndLibrary(Pageable pageable,
			                                       @Param("library") Library library,
												    @Param("searchPhrase") String searchPhrase);

	@Query("Select count(*) "
		 + "from BookWaitListItem b "
		 + "where (:library is null or b.library=:library) "
		 + "  and (:book is null or b.book=:book) "
		 + "group by b.book, b.library")
	List<Long> findWaitListCount(@Param("library") Library library, @Param("book") Book book);

	
	@Query("Select b "
		 + "from BookWaitListItem bw "
		 + "join bw.user u "
		 + "join bw.book b "
		 + "left outer join bw.library l "
		 + "where u.userId=:userId "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId) "
		 + "group by b")
		List<Book> findCurrentUserUniqueWaitListItems(@Param("userId") Long userId,
												      @Param("searchPhrase") String searchPhrase, 
												      @Param("libraryId") Long libraryId,
												      Pageable pageable);
	
	@Query("Select bw "
		 + "from BookWaitListItem bw "
		 + "join bw.user u "
		 + "join bw.book b "
		 + "left outer join bw.library l "
		 + "where u.userId=:userId "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId)")
	List<BookWaitListItem> findUserWaitList(@Param("userId") Long userId,
                                            @Param("searchPhrase") String searchPhrase, 
										    @Param("libraryId") Long libraryId, 
										    Pageable pageable);
	
	@Query("Select count(bw) "
		 + "from BookWaitListItem bw "
		 + "join bw.user u "
		 + "join bw.book b "
		 + "left join bw.library l "
		 + "where u.userId=:userId "
		 + "  and (:searchPhrase is null "
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publicationYear) like lower(concat('%',:searchPhrase,'%'))) "
		 + "  and (:libraryId is null or l.libraryId=:libraryId)")
	Long findUserWaitListCount(@Param("userId") Long userId,
							   @Param("searchPhrase") String searchPhrase, 
							   @Param("libraryId") Long libraryId);



	List<BookWaitListItem> findAllByBookAndLibrary(Book book, Library library);

	void deleteByBook(Book book);

	void deleteByUser(User user);

	void deleteByLibrary(Library library);

	BookWaitListItem findOneByWaitListId(Long waitListId);



}
