package com.example.ibdb.repository;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.Rating;
import com.example.ibdb.domain.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ratingRepository extends JpaRepository<Rating, Long> {

    @Query("select r "
    	 + "from Rating r "
    	 + "join r.user u "
    	 + "join r.book b "
    	 + "where b.isbn=:isbn" 
    	 + " and u.userId=:userId")
    Rating findCurrentUserRating(@Param("isbn") String isbn, @Param("userId") Long userId);

    @Query("select count(r) "
    	 + "from Rating r "
    	 + "join r.user u "
    	 + "join r.book b "
    	 + "where u.userId=:userId "
    	 + "  and (:searchPhrase is null"
		 + "   or lower(b.isbn) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.publisher) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookAuthor) like lower(concat('%',:searchPhrase,'%')) "
		 + "   or lower(b.bookTitle) like lower(concat('%',:searchPhrase,'%')) "
    	 + "   or b.publicationYear like lower(concat('%',:searchPhrase,'%')))")
    Long findCurrentUserRatingCount(@Param("userId") Long userId, @Param("searchPhrase") String searchPhrase);

    @Query("select avg(r.rating) "
    	 + "from Book b "
    	 + "join b.ratings r "
    	 + "join r.user u "
    	 + "where b.isbn=:isbn "
    	 + "  and u.birthday>=:startAge " 
    	 + "  and u.birthday<=:endAge"
    	 + " group by b")
    Double findRating(@Param("isbn") String isbn, @Param("startAge") LocalDate startAge, @Param("endAge") LocalDate endAge); 
    
    void deleteByBook(Book book);
    void deleteByUser(User user);

	void deleteAllByBookAndUser(Book book, User user);

	Rating findOneByRatingId(Long ratingId);

	Rating findOneByBookAndUser(Book book, User user);
}
