package com.example.ibdb.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ibdb.domain.Library;

@Repository
public interface libraryRepository extends JpaRepository<Library, Long> {
	List<Library> findAll();
	
	@Query("Select l from Library l")
	List<Library> findAllWithPagination(Pageable pageable);
	
	@Query("Select l from BookCopy bc join bc.library l where bc.bookId=:bookId")
	Library findOneByBookId(@Param("bookId") Long bookId);
	
	@Query("Select count(l) from Library l")
	Long getLibraryCount();
	
	Library findOneByLibraryId(Long libraryId);
}
