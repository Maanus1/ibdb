package com.example.ibdb.service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.Rating;
import com.example.ibdb.domain.User;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.repository.ratingRepository;
import com.example.ibdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

@Service
public class RatingService {
	@Autowired
	ratingRepository ratingRepository;

	@Autowired
	bookRepository bookRepository;

	@Autowired
	UserService userService;

	@Autowired
	LentBookService lentBookService;

	public Map<String, Object> getRatings(Integer pageNumber, Integer pageSize, String searchPhrase, Long startAge,
			Long endAge, Double minimumRating, Long libraryId) {
		List<Book> bookRatings = bookRepository.findRatings(PageRequest.of(pageNumber - 1, pageSize),
				UtilsService.getbirthYearFromAge(endAge), UtilsService.getbirthYearFromAge(startAge), searchPhrase,
				minimumRating);
		for (Book book : bookRatings) {
			book.setAvailable(lentBookService.isAvailable(book.getIsbn(), libraryId));
		}
		Long bookCount = bookRepository.findBookCount(UtilsService.getbirthYearFromAge(endAge),
				UtilsService.getbirthYearFromAge(startAge), searchPhrase, minimumRating);
		Map<String, Object> bookMap = new HashMap<>();
		bookMap.put("bookCount", bookCount);
		bookMap.put("books", bookRatings);
		return bookMap;
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public void deleteRating(Long ratingId) {
		ratingRepository.deleteById(ratingId);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public void updateRating(Rating rating) {
		if (userService.getCurrentUser() != userService.getUserByUserId(rating.getUser().getUserId())) {
			throw new FieldException(TranslationService.translate("error.permission.changeRating", null));
		}
		Rating oldRating = ratingRepository.findOneByRatingId(rating.getRatingId());
		oldRating.setRating(rating.getRating());
		ratingRepository.save(oldRating);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	@Transactional
	public Rating addRating(Rating rating) {
		User user = userService.getCurrentUser();
		rating.setUser(user);
		Rating oldRating = ratingRepository.findOneByBookAndUser(rating.getBook(), user);
		if (oldRating != null) {
			oldRating.setRating(rating.getRating());
			return ratingRepository.save(oldRating);
		}
		return ratingRepository.save(rating);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Long getCurrentUserRatingCount(String searchPhrase) {
		User user = userService.getCurrentUser();
		return ratingRepository.findCurrentUserRatingCount(user.getUserId(), searchPhrase);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<Rating> getCurrentUserRatings(Integer pageNumber, Integer pageSize, String searchPhrase) {
		User user = userService.getCurrentUser();
		return bookRepository.findCurrentUserRatings(PageRequest.of(pageNumber - 1, pageSize), user.getUserId(),
				searchPhrase);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Rating getCurrentUserRating(String isbn) {
		User user = userService.getCurrentUser();
		return ratingRepository.findCurrentUserRating(isbn, user.getUserId());
	}

	public Double getRating(String isbn, Long startAge, Long endAge) {
		return ratingRepository.findRating(isbn, UtilsService.getbirthYearFromAge(endAge),
				UtilsService.getbirthYearFromAge(startAge));
	}

	public void deleteByBook(Book book) {
		ratingRepository.deleteByBook(book);
	}
}
