package com.example.ibdb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

@Service
public class UtilsService {

	public static LocalDate getbirthYearFromAge(Long age) {
		return age == null ? null : LocalDate.now().minusYears(age);
	}

	public static List<LocalDate> getAllDatesBetweenTwoDates(LocalDate fromDate, LocalDate toDate) {
		long numOfDaysBetween = ChronoUnit.DAYS.between(fromDate, toDate.plusDays(1));
		return IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween).mapToObj(i -> fromDate.plusDays(i))
				.collect(Collectors.toList());
	}
}
