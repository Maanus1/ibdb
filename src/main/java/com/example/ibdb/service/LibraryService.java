package com.example.ibdb.service;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Library;
import com.example.ibdb.repository.libraryRepository;
import com.example.ibdb.repository.bookWaitListItemRepository;
import com.example.ibdb.repository.lentBookRepository;

@Service
public class LibraryService {

	@Autowired
	libraryRepository libraryRepository;

	@Autowired
	bookWaitListItemRepository bookWaitListItemRepository;

	@Autowired
	lentBookRepository lentBookRepository;

	@Autowired
	BookCopyService bookCopyService;

	public List<Library> getAllLibraries() {
		return libraryRepository.findAll();
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public List<Library> getAllLibrariesWithPagination(Integer pageNumber, Integer pageSize) {
		return libraryRepository.findAllWithPagination(PageRequest.of(pageNumber - 1, pageSize));
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public Library getLibrary(Long libraryId) {
		return libraryRepository.findOneByLibraryId(libraryId);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Library addLibrary(Library library) {
		return libraryRepository.save(library);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Library updateLibrary(Library updatedLibrary) {
		Library library = libraryRepository.findOneByLibraryId(updatedLibrary.getLibraryId());
		library.setAddress(updatedLibrary.getAddress());
		library.setLatitude(updatedLibrary.getLatitude());
		library.setLongitude(updatedLibrary.getLongitude());
		library.setOpeningTime(updatedLibrary.getOpeningTime());
		library.setClosingTime(updatedLibrary.getClosingTime());
		return libraryRepository.save(library);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public Long getLibrarycount() {
		return libraryRepository.getLibraryCount();
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Transactional
	public void deleteLibrary(Long libraryId) {
		Library library = libraryRepository.findOneByLibraryId(libraryId);
		bookWaitListItemRepository.deleteByLibrary(library);
		if (this.bookCopyService.getLibraryBookCopyCount(libraryId) != 0) {
			throw new FieldException(TranslationService.translate("error.library.containsBookCopies", null));
		}
		lentBookRepository.deleteByLibrary(library);
		libraryRepository.delete(library);
	}
}
