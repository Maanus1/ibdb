package com.example.ibdb.service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.User;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.repository.bookCopyRepository;
import com.example.ibdb.repository.bookWaitListItemRepository;
import com.example.ibdb.repository.ratingRepository;
import com.example.ibdb.repository.userRepository;
import com.example.ibdb.repository.notificationRepository;
import com.example.ibdb.service.BookCopyService;
import com.example.ibdb.service.BookCommentService;
import com.example.ibdb.service.LentBookService;
import com.example.ibdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

@Service
public class BookService {
	@Autowired
	bookRepository bookRepository;

	@Autowired
	ratingRepository ratingRepository;

	@Autowired
	bookCopyRepository bookCopyRepository;

	@Autowired
	BookCopyService bookCopyService;

	@Autowired
	BookCommentService bookCommentService;

	@Autowired
	LentBookService lentBookService;

	@Autowired
	UserService userService;

	@Autowired
	userRepository userRepository;

	@Autowired
	notificationRepository notificationRepository;

	@Autowired
	bookWaitListItemRepository bookWaitListItemRepository;

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<Book> getBooks(Integer pageNumber, Integer pageSize, String searchPhrase) {
		return bookRepository.findAllBooks(PageRequest.of(pageNumber - 1, pageSize), searchPhrase);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Boolean isIsbnTaken(String isbn) {
		return bookRepository.findOneByIsbn(isbn) != null ? true : false;
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Long findAllBooksCount(String searchPhrase) {
		return bookRepository.findAllBooksCount(searchPhrase);
	}

	public Book getBookByIsbn(String isbn) {
		return bookRepository.findOneByIsbn(isbn);
	}

	public Long findBookCount(String searchPhrase, Long startAge, Long endAge, Double minimumRating) {
		return bookRepository.findBookCount(UtilsService.getbirthYearFromAge(endAge),
				UtilsService.getbirthYearFromAge(startAge), searchPhrase, minimumRating);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	@Transactional
	public void deleteBook(String isbn) {
		Book book = bookRepository.findOneByIsbn(isbn);
		if (bookCopyRepository.findAllByBook(book).size() > 0) {
			throw new FieldException(TranslationService.translate("error.book.hasBookCopies", null));
		}
		ratingRepository.deleteByBook(book);
		notificationRepository.deleteByBook(book);
		bookWaitListItemRepository.deleteByBook(book);
		bookCommentService.deleteBookComments(book);
		bookRepository.deleteByIsbn(isbn);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Book updateBook(Book book) {
		return bookRepository.save(book);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public Map<String, Object> getRecommendedBooks(Integer pageNumber, Integer pageSize, Long startAge, Long endAge,
			String filterPhrase, Long libraryId) {
		User user = userService.getCurrentUser();
		List<Book> userBooks = bookRepository.findTop10CurrentUserHighestRatedBooks(user.getUserId());
		List<Long> userIds = new ArrayList<>();
		for (Book book : userBooks) {
			userIds.addAll(bookRepository.findTop10UsersWithHighestRatings(book.getIsbn(), user.getUserId()));
		}
		Map<Long, Long> sortedByCountSet = userIds.stream()
				.collect(Collectors.groupingBy(id -> id, TreeMap::new, Collectors.counting())).entrySet().stream()
				.sorted(Comparator.comparing(Map.Entry<Long, Long>::getValue).reversed())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		Set<String> recommendedBookIsbns = new HashSet<>();
		for (Entry<Long, Long> entry : sortedByCountSet.entrySet()) {
			recommendedBookIsbns
					.addAll(bookRepository.findTop3UserHighestRatedBooks((Long) entry.getKey(), user.getUserId()));
		}
		List<Book> recommendedBooks = bookRepository.findBooksByIsbn(recommendedBookIsbns,
				PageRequest.of(pageNumber - 1, pageSize), UtilsService.getbirthYearFromAge(startAge),
				UtilsService.getbirthYearFromAge(endAge), filterPhrase);
		Long recommendedBookCount = bookRepository.findBookCountByIsbn(recommendedBookIsbns);
		for (Book book : recommendedBooks) {
			book.setAvailable(lentBookService.isAvailable(book.getIsbn(), libraryId));
		}
		Map<String, Object> recommendedBookMap = new HashMap<>();
		recommendedBookMap.put("bookCount", recommendedBookCount);
		recommendedBookMap.put("books", recommendedBooks);
		return recommendedBookMap;
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Book addBook(Book book) {
		if (bookRepository.findOneByIsbn(book.getIsbn()) != null) {
			throw new FieldException(TranslationService.translate("error.book.isbnTaken", null));
		}
		return bookRepository.save(book);
	}
	
	public List<Book> getTopRatedBooks(Integer topSize, Long startAge, Long endAge, Long libraryId) {
		List<Book> topBooks = bookRepository.findTopRatings(PageRequest.of(0, topSize),
				UtilsService.getbirthYearFromAge(endAge), UtilsService.getbirthYearFromAge(startAge));
		for (Book book : topBooks) {
			book.setAvailable(lentBookService.isAvailable(book.getIsbn(), libraryId));
		}
		return topBooks;
	}
	
	public Book getBookWithRating(String isbn, Long libraryId) {
		Book book = bookRepository.findBookWithRating(isbn);
		book.setAvailable(lentBookService.isAvailable(isbn, libraryId));
		return book;
	}

}
