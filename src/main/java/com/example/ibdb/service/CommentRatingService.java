package com.example.ibdb.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.BookComment;
import com.example.ibdb.domain.CommentRating;
import com.example.ibdb.domain.User;
import com.example.ibdb.repository.commentRatingRepository;
import com.example.ibdb.repository.bookCommentRepository;

@Service
public class CommentRatingService {

	@Autowired
	commentRatingRepository commentRatingRepository;

	@Autowired
	bookCommentRepository bookCommentRepository;

	@Autowired
	UserService userService;

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@Transactional
	public void deleteCommentRating(Long id) {
		User user = userService.getUserByUserId(commentRatingRepository.findOneById(id).getUser());
		if (userService.getCurrentUser() != user) {
			throw new FieldException(TranslationService.translate("error.permission.changeRating", null));
		}
		this.commentRatingRepository.deleteById(id);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public CommentRating addCommentRating(CommentRating commentRating) {
		commentRating.setUser(this.userService.getCurrentUser());
		return commentRatingRepository.save(commentRating);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public CommentRating updateCommentRating(CommentRating commentRating) {
		User user = userService.getUserByUserId(commentRatingRepository.findOneById(commentRating.getId()).getUser());
		if (userService.getCurrentUser() != user) {
			throw new FieldException(TranslationService.translate("error.permission.changeRating", null));
		}
		CommentRating oldCommentRating = commentRatingRepository.findOneById(commentRating.getId());
		oldCommentRating.setRating(commentRating.getRating());
		return commentRatingRepository.save(oldCommentRating);
	}

}
