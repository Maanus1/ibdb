package com.example.ibdb.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.ibdb.service.UserService;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.Notification;
import com.example.ibdb.domain.User;
import com.example.ibdb.model.NotificationType;
import com.example.ibdb.repository.notificationRepository;

@Service
public class NotificationService {

	@Autowired
	notificationRepository notificationRepository;

	@Autowired
	UserService userService;

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Notification addNotification(User user, NotificationType notificationType) {
		Notification notification = new Notification(user, notificationType);
		return this.saveNotification(notification);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Notification addNotification(User user, NotificationType notificationType, Book book) {
		Notification notification = new Notification(user, notificationType, book);
		return this.saveNotification(notification);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Notification addNotification(User user, NotificationType notificationType, Book book, Library library) {
		Notification notification = new Notification(user, notificationType, book);
		return this.saveNotification(notification);
	}

	public Notification saveNotification(Notification notification) {
		notification.setTimestamp(LocalDateTime.now());
		return notificationRepository.save(notification);
	}

	public Notification addNotificationScheduled(Notification notification) {
		return notificationRepository.save(notification);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<Notification> getUserNotifications(Integer pageNumber, Integer pageSize) {
		User user = userService.getCurrentUser();
		List<Notification> notifications = notificationRepository
				.findAllByUser(PageRequest.of(pageNumber - 1, pageSize), user);
		for (Notification notification : notifications) {
			String bookTitle = notification.getBook() != null ? notification.getBook().getBookTitle() : null;
			String libraryAddress = notification.getLibrary() != null ? notification.getLibrary().getAddress() : null;
			notification.setMessage(TranslationService.translate(notification.getNotificationType().getMessage(),
					new Object[] { bookTitle, libraryAddress }));
		}
		return notifications;
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	@Transactional
	public void markNotificationsAsSeen() {
		User user = userService.getCurrentUser();
		notificationRepository.markNotificationsAsSeen(user);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Long getNotificationCount() {
		User user = userService.getCurrentUser();
		return notificationRepository.getNotificationCount(user);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Long getUnreadNotificationCount() {
		User user = userService.getCurrentUser();
		return notificationRepository.getUnreadNotificationCount(user);
	}
}
