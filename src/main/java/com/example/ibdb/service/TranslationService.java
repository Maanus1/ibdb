package com.example.ibdb.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

@Service
public class TranslationService {
	private static ResourceBundleMessageSource messageSource;

	@Autowired
	TranslationService(ResourceBundleMessageSource messageSource) {
		TranslationService.messageSource = messageSource;
	}

	public static String translate(String msgCode, Object[] args) {
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(msgCode, args, locale);
	}
}
