package com.example.ibdb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.LentBook;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.User;
import com.example.ibdb.model.BookLendingStatus;
import com.example.ibdb.model.NotificationType;
import com.example.ibdb.repository.lentBookRepository;
import com.example.ibdb.repository.libraryRepository;
import com.example.ibdb.repository.bookCopyRepository;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.repository.notificationRepository;
import com.example.ibdb.service.NotificationService;
import com.example.ibdb.service.UserService;
import com.example.ibdb.service.TranslationService;

@Service
public class LentBookService {

	@Autowired
	lentBookRepository lentBookRepository;

	@Autowired
	bookCopyRepository bookCopyRepository;

	@Autowired
	bookRepository bookRepository;

	@Autowired
	notificationRepository notificationRepository;

	@Autowired
	libraryRepository libraryRepository;

	@Autowired
	NotificationService notificationService;

	@Autowired
	UserService userService;

	@Value("${lending.maxDays}")
	private Long maxLendingDays;

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<LentBook> getBookCopyLendingData(Long bookId) {
		BookCopy bookCopy = bookCopyRepository.findOneByBookId(bookId);
		LocalDate today = LocalDate.now();
		return lentBookRepository.findAllByBookCopy(bookCopy, today);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<LocalDate> getBookReservedDays(Long bookId) {
		BookCopy bookCopy = bookCopyRepository.findOneByBookId(bookId);
		LocalDate today = LocalDate.now();
		List<LentBook> lentBooks = lentBookRepository.findAllByBookCopy(bookCopy, today);
		List<LocalDate> reservedDates = new ArrayList<>();
		for (LentBook book : lentBooks) {
			reservedDates.addAll(UtilsService.getAllDatesBetweenTwoDates(book.getFromDate(), book.getToDate()));
		}
		return reservedDates;
	}

	public boolean isAvailable(String isbn, Long libraryId) {
		List<BookCopy> bookCopies = bookCopyRepository.findAllByBookForUser(bookRepository.findOneByIsbn(isbn),
				libraryId);
		LocalDate today = LocalDate.now();
		List<LocalDate> allDates = UtilsService.getAllDatesBetweenTwoDates(today,
				today.plusMonths(6).withDayOfMonth(1));
		Set<LocalDate> reservedDates = new HashSet<>();
		for (BookCopy bookCopy : bookCopies) {
			List<LentBook> lentBooks = lentBookRepository.findAllByBookCopy(bookCopy, today);
			for (LentBook book : lentBooks) {
				reservedDates.addAll(UtilsService.getAllDatesBetweenTwoDates(book.getFromDate(), book.getToDate()));
			}
			for (LocalDate date : allDates) {
				if (!reservedDates.contains(date)) {
					return true;
				}
			}
			reservedDates.clear();
		}
		return false;
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public LentBook addLentBook(LentBook lentBook) {
		if (bookCopyRepository.findOneByLibraryAndBookId(lentBook.getLibrary(),
				lentBook.getBookCopy().getBookId()) == null) {
			throw new FieldException(TranslationService.translate("error.lending.invalidBookCopy", null));
		}
		if (lentBook.getFromDate().isAfter(lentBook.getToDate()) || lentBook.getFromDate().isBefore(LocalDate.now())) {
			throw new FieldException(TranslationService.translate("error.lending.reservationPeriod", null));
		}
		if (ChronoUnit.DAYS.between(lentBook.getFromDate(), lentBook.getToDate()) > maxLendingDays - 1) {
			throw new FieldException(TranslationService.translate("error.lending.reservationPeriodLength",
					new Object[] { maxLendingDays - 1 }));
		}
		List<LentBook> lentBooks = lentBookRepository.findAllByBookCopyBetweenDates(lentBook.getBookCopy(),
				lentBook.getFromDate(), lentBook.getToDate());
		List<LocalDate> dates = new ArrayList<>();
		for (LentBook book : lentBooks) {
			dates.addAll(UtilsService.getAllDatesBetweenTwoDates(book.getFromDate(), book.getToDate()));
		}
		for (LocalDate date : UtilsService.getAllDatesBetweenTwoDates(lentBook.getFromDate(), lentBook.getToDate())) {
			if (dates.contains(date)) {
				throw new FieldException(TranslationService.translate("error.lending.reservationPeriod", null));
			}
		}
		User user = userService.getCurrentUser();
		lentBook.setUser(user);
		lentBook.setTimestamp(LocalDateTime.now());
		return lentBookRepository.save(lentBook);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public void deleteBookCopy(LentBook lentBook) {
		lentBookRepository.delete(lentBook);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public LentBook updateLentBookStatus(Long lentBookId) {
		LentBook lentBook = lentBookRepository.findOneById(lentBookId);
		Book book = bookCopyRepository.findOneByBookId(lentBook.getBookCopy().getBookId()).getBook();
		switch (lentBook.getBookLendingStatus()) {
		case BOOK_RESERVED:
			lentBook.setBookLendingStatus(BookLendingStatus.ACTIVE);
			notificationService.addNotification(lentBook.getUser(), NotificationType.LENDING_ACTIVATED, book);
			break;
		case ACTIVE:
			lentBook.setBookLendingStatus(BookLendingStatus.BOOK_RETURNED);
			notificationService.addNotification(lentBook.getUser(), NotificationType.BOOK_RETURNED, book);
			break;
		default:
			break;
		}
		lentBook = lentBookRepository.save(lentBook);
		return lentBook;
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public LentBook cancelBookLending(Long lentBookId) {
		LentBook lentBook = lentBookRepository.findOneById(lentBookId);
		if (lentBook.getUser().getUserId() != userService.getCurrentUser().getUserId()) {
			throw new FieldException(TranslationService.translate("error.permission.changeBook", null));
		}
		lentBook.setBookLendingStatus(BookLendingStatus.CANCELLED);
		lentBook = lentBookRepository.save(lentBook);
		return lentBook;
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public LentBook cancelBookLendingByAdmin(Long lentBookId) {
		LentBook lentBook = lentBookRepository.findOneById(lentBookId);
		lentBook.setBookLendingStatus(BookLendingStatus.CANCELLED);
		lentBook = lentBookRepository.save(lentBook);
		User user = userService.getCurrentUser();
		Book book = bookCopyRepository.findOneByBookId(lentBook.getBookCopy().getBookId()).getBook();
		if (lentBook.getUser().getUserId() != user.getUserId()) {
			notificationService.addNotification(lentBook.getUser(), NotificationType.RESERVATION_CANCELLED, book);
		}
		return lentBook;
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public LentBook updateBookCopy(LentBook lentBook) {
		return lentBookRepository.save(lentBook);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<BookLendingStatus> getLendingStatuses() {
		User user = userService.getCurrentUser();
		return lentBookRepository.findCurrentUserLendingStatuses(user.getUserId());
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<String> getAllLendingStatuses() {
		return Stream.of(BookLendingStatus.values()).map(Enum::name).collect(Collectors.toList());
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public Long getCurrentUserLentBookCount(String searchPhrase, String lendingStatus, LocalDate fromDate,
			LocalDate toDate, Long libraryId) {
		User user = userService.getCurrentUser();
		return lentBookRepository.findCurrentUserLentBookCount(user.getUserId(), searchPhrase,
				BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<LentBook> getCurrentUserLentBooks(Integer pageNumber, Integer pageSize, String searchPhrase,
			String lendingStatus, LocalDate fromDate, LocalDate toDate, Long libraryId) {
		User user = userService.getCurrentUser();
		return lentBookRepository.findCurrentUserLentBooks(PageRequest.of(pageNumber - 1, pageSize), user.getUserId(),
				searchPhrase, BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public List<Book> getCurrentUserUniqueLentBooks(Integer pageNumber, Integer pageSize, String searchPhrase,
			String lendingStatus, LocalDate fromDate, LocalDate toDate, Long libraryId) {
		User user = userService.getCurrentUser();
		return lentBookRepository.findCurrentUserUniqueLentBooks(PageRequest.of(pageNumber - 1, pageSize),
				user.getUserId(), searchPhrase, BookLendingStatus.fromValue(lendingStatus), fromDate, toDate,
				libraryId);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Long getLentBookCount(String bookSearchPhrase, String userSearchPhrase, String lendingStatus,
			LocalDate fromDate, LocalDate toDate, Long libraryId) {
		return lentBookRepository.findLentBookCount(bookSearchPhrase, userSearchPhrase,
				BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<LentBook> getLentBooks(Integer pageNumber, Integer pageSize, String bookSearchPhrase,
			String userSearchPhrase, String lendingStatus, LocalDate fromDate, LocalDate toDate, Long libraryId) {
		return lentBookRepository.findLentBooks(PageRequest.of(pageNumber - 1, pageSize), bookSearchPhrase,
				userSearchPhrase, BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<User> getUniqueLentBookUsers(Integer pageNumber, Integer pageSize, String bookSearchPhrase,
			String userSearchPhrase, String lendingStatus, LocalDate fromDate, LocalDate toDate, Long libraryId) {
		return lentBookRepository.findUniqueLentBookUsers(PageRequest.of(pageNumber - 1, pageSize), bookSearchPhrase,
				userSearchPhrase, BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<Book> getUniqueLentBookBooks(Integer pageNumber, Integer pageSize, String bookSearchPhrase,
			String userSearchPhrase, String lendingStatus, LocalDate fromDate, LocalDate toDate, Long libraryId) {
		return lentBookRepository.findUniqueLentBookBooks(PageRequest.of(pageNumber - 1, pageSize), bookSearchPhrase,
				userSearchPhrase, BookLendingStatus.fromValue(lendingStatus), fromDate, toDate, libraryId);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public void deleteByBookCopy(BookCopy bookCopy) {
		lentBookRepository.deleteByBookCopy(bookCopy);
	}
}
