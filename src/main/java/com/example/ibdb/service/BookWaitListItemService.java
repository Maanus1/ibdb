package com.example.ibdb.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.example.ibdb.Constants;
import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookWaitListItem;
import com.example.ibdb.domain.Library;
import com.example.ibdb.domain.Notification;
import com.example.ibdb.domain.User;
import com.example.ibdb.model.NotificationType;
import com.example.ibdb.repository.bookWaitListItemRepository;
import com.example.ibdb.repository.libraryRepository;
import com.example.ibdb.repository.userRepository;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.service.UserService;

@Service
public class BookWaitListItemService {

	@Autowired
	userRepository userRepository;

	@Autowired
	bookWaitListItemRepository bookWaitListItemRepository;

	@Autowired
	libraryRepository libraryRepository;

	@Autowired
	LentBookService lentBookService;

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	bookRepository bookRepository;

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public BookWaitListItem addToWaitList(BookWaitListItem waitListItem) {
		waitListItem.setUser(userService.getCurrentUser());
		if (bookWaitListItemRepository.findOneByLibraryAndBookAndUser(waitListItem.getLibrary(), waitListItem.getBook(),
				waitListItem.getUser()) != null) {
			throw new FieldException(TranslationService.translate("error.notification.notificationExists", null));
		}
		return bookWaitListItemRepository.save(waitListItem);

	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public void deleteBookWaitListItem(Long waitListId) {
		if (userService.getCurrentUser() != bookWaitListItemRepository.findOneByWaitListId(waitListId).getUser()) {
			throw new FieldException(TranslationService.translate("error.permission.deleteWaitListItem", null));
		}
		bookWaitListItemRepository.deleteById(waitListId);

	}

	@Scheduled(fixedDelay = 1000/* cron = Constants.WAIT_LIST_AVAILABILITY_CHECK_INTERVAL */)
	public void checkBookWaitList() {
		List<BookWaitListItem> waitList = bookWaitListItemRepository.findAllBooksAndLibraries();
		for (BookWaitListItem item : waitList) {
			this.addBookAvailableNotification(item);
		}
	}

	public void addBookAvailableNotification(BookWaitListItem item) {
		Long libraryId = item.getLibrary() != null ? item.getLibrary().getLibraryId() : null;
		if (lentBookService.isAvailable(item.getBook().getIsbn(), libraryId)) {
			List<BookWaitListItem> waitList = bookWaitListItemRepository.findAllByBookAndLibrary(item.getBook(),
					item.getLibrary());
			for (BookWaitListItem bookWaitListItem : waitList) {
				Notification notification = new Notification(bookWaitListItem.getUser(),
						NotificationType.BOOK_AVAILABLE, bookWaitListItem.getBook());
				notification.setTimestamp(LocalDateTime.now());
				notificationService.addNotificationScheduled(notification);
				bookWaitListItemRepository.delete(bookWaitListItem);
			}
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public BookWaitListItem getWaitListItem(Long libraryId, String isbn) {
		User user = userService.getCurrentUser();
		Book book = bookRepository.findOneByIsbn(isbn);
		Library library = libraryRepository.findOneByLibraryId(libraryId);
		return bookWaitListItemRepository.findOneByLibraryAndBookAndUser(library, book, user);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public List<BookWaitListItem> getWaitList(Integer pageNumber, Integer pageSize, Long libraryId,
			String searchPhrase) {
		Library library = libraryRepository.findOneByLibraryId(libraryId);
		return bookWaitListItemRepository.findAllByBookAndLibrary(PageRequest.of(pageNumber - 1, pageSize), library,
				searchPhrase);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public Long getWaitListCount(Long libraryId, String isbn) {
		Book book = bookRepository.findOneByIsbn(isbn);
		Library library = libraryRepository.findOneByLibraryId(libraryId);
		return new Long(bookWaitListItemRepository.findWaitListCount(library, book).size());
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public List<BookWaitListItem> getUserWaitList(Integer pageNumber, Integer pageSize, Long libraryId,
			String searchPhrase) {
		User user = userService.getCurrentUser();
		return bookWaitListItemRepository.findUserWaitList(user.getUserId(), searchPhrase, libraryId,
				PageRequest.of(pageNumber - 1, pageSize));
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public List<Book> getUserWaitListUniqueBooks(Integer pageNumber, Integer pageSize, Long libraryId,
			String searchPhrase) {
		User user = userService.getCurrentUser();
		return bookWaitListItemRepository.findCurrentUserUniqueWaitListItems(user.getUserId(), searchPhrase, libraryId,
				PageRequest.of(pageNumber - 1, pageSize));
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_USER')")
	public Long getUserWaitListCount(Long libraryId, String searchPhrase) {
		User user = userService.getCurrentUser();
		return bookWaitListItemRepository.findUserWaitListCount(user.getUserId(), searchPhrase, libraryId);
	}
}
