package com.example.ibdb.service;

import com.example.ibdb.model.NotificationType;
import com.example.ibdb.model.RoleName;
import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Role;
import com.example.ibdb.domain.User;
import com.example.ibdb.repository.bookWaitListItemRepository;
import com.example.ibdb.repository.lentBookRepository;
import com.example.ibdb.repository.ratingRepository;
import com.example.ibdb.repository.userRepository;
import com.example.ibdb.repository.roleRepository;
import com.example.ibdb.repository.notificationRepository;
import com.example.ibdb.service.BookCommentService;
import com.example.ibdb.repository.libraryRepository;
import com.example.ibdb.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	userRepository userRepository;

	@Autowired
	lentBookRepository lentBookRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	roleRepository roleRepository;

	@Autowired
	ratingRepository ratingRepository;

	@Autowired
	notificationRepository notificationRepository;

	@Autowired
	NotificationService notificationService;

	@Autowired
	BookCommentService bookCommentService;

	@Autowired
	libraryRepository libraryRepository;

	@Autowired
	bookWaitListItemRepository bookWaitListItemRepository;

	@PostConstruct
	public void initialize() {
		if (roleRepository.findAll().size() == 0) {
			Arrays.stream(RoleName.values()).forEach(x -> roleRepository.save(new Role(x)));
		}
		Role userRole = roleRepository.findByName(RoleName.ROLE_USER);
		Role employeeRole = roleRepository.findByName(RoleName.ROLE_EMPLOYEE);

		Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN);

		if (userRepository.findOneByEmail("user@u.u") == null) {
			createUser(new User("user@u.u", "user", "b", "b", LocalDate.of(1990, 1, 1), "user",
					Collections.singletonList(userRole)));
		}
		if (userRepository.findOneByEmail("admin@a.a") == null) {
			createUser(new User("admin@a.a", "admin", "b", "b", LocalDate.of(1980, 1, 1), "admin",
					Arrays.asList(adminRole, userRole, employeeRole)));
		}
		if (userRepository.findOneByEmail("employee@e.e") == null) {
			createUser(new User("employee@e.e", "employee", "b", "b", LocalDate.of(1970, 1, 1), "employee",
					Arrays.asList(employeeRole, userRole)));
		}
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public User changePassword(String currentPassword, String newPassword) {
		User user = userRepository
				.findOneUserByEmail(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		if (passwordEncoder.matches(currentPassword, user.getPassword())) {
			user.setPassword(passwordEncoder.encode(newPassword));
			return userRepository.save(user);
		} else {
			throw new FieldException(TranslationService.translate("error.user.invalidPassword", null));
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Role> getRoles() {
		return roleRepository.findAll();
	}

	public Boolean isEmailTaken(String email, Long userId) {
		User user = userRepository.findOneByEmail(email);
		return user != null && !(userId == user.getUserId()) ? true : false;
	}

	public Boolean isAliasTaken(String alias, Long userId) {
		User user = userRepository.findOneByAlias(alias);
		return user != null && !(userId == user.getUserId()) ? true : false;
	}

	@PreAuthorize("hasRole('ROLE_USER')  or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public User getCurrentUser() {
		return userRepository
				.findOneUserByEmail(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
	}

	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public User getUserByUserId(Long userId) {
		return userRepository.findOneByUserId(userId);
	}

	@PreAuthorize("hasRole('ROLE_USER')  or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	public User updateUser(User newUser) {
		User user = this.getCurrentUser();
		if (newUser.getPreferredLibrary() != null) {
			newUser.setPreferredLibrary(
					libraryRepository.findOneByLibraryId(newUser.getPreferredLibrary().getLibraryId()));
		}
		newUser.setPassword(user.getPassword());
		newUser.setEmail(user.getEmail());
		newUser.setUserId(user.getUserId());
		newUser.setRoles(user.getRoles());
		return userRepository.save(newUser);
	}

	@PreAuthorize("hasRole('ROLE_USER')  or hasRole('ROLE_EMPLOYEE') or  hasRole('ROLE_ADMIN')")
	@Transactional
	public void deleteUser(Long userId) {
		User user = userRepository.findOneByUserId(userId);
		ratingRepository.deleteByUser(user);
		lentBookRepository.deleteByUser(user);
		notificationRepository.deleteByUser(user);
		bookWaitListItemRepository.deleteByUser(user);
		bookCommentService.deleteUserComments(user);
		userRepository.deleteById(userId);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User updateUserByAdmin(User newUser) {
		if (this.isEmailTaken(newUser.getAlias(), newUser.getUserId())) {
			throw new FieldException(
					TranslationService.translate("error.user.emailTaken", new Object[] { newUser.getEmail() }));
		}
		if (this.isAliasTaken(newUser.getAlias(), newUser.getUserId())) {
			throw new FieldException(
					TranslationService.translate("error.user.aliasTaken", new Object[] { newUser.getAlias() }));
		}
		User user = userRepository.findOneByUserId(newUser.getUserId());
		newUser.setUserId(user.getUserId());
		newUser.setPassword(user.getPassword());
		notificationService.addNotification(user, NotificationType.USER_INFORMATION);
		return userRepository.save(newUser);
	}

	public User createUser(User user) {
		if (this.isEmailTaken(user.getAlias(), user.getUserId())) {
			throw new FieldException(
					TranslationService.translate("error.user.emailTaken", new Object[] { user.getEmail() }));
		}
		if (this.isAliasTaken(user.getAlias(), user.getUserId())) {
			throw new FieldException(
					TranslationService.translate("error.user.aliasTaken", new Object[] { user.getAlias() }));
		}
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		if (user.getRoles() == null) {
			user.setRoles(new HashSet<>(Collections.singletonList(roleRepository.findByName(RoleName.ROLE_USER))));
		}
		return userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepository.findOneByEmail(email);
		if (user == null) {
			throw new UsernameNotFoundException(
					TranslationService.translate("error.user.invalidEmailOrPassword", null));
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				getAuthority(user));
	}

	private List<SimpleGrantedAuthority> getAuthority(User user) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		for (Role role : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName().toString()));
		}
		return authorities;
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> getUsers(Integer pageNumber, Integer pageSize, String searchPhrase) {
		return userRepository.findAllUsers(PageRequest.of(pageNumber - 1, pageSize), searchPhrase);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Long findUserCount(String searchPhrase) {
		return userRepository.findUserCount(searchPhrase);
	}
}
