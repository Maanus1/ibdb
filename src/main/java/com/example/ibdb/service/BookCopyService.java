package com.example.ibdb.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.Library;
import com.example.ibdb.repository.bookCopyRepository;
import com.example.ibdb.service.BookService;
import com.example.ibdb.service.LentBookService;
import com.example.ibdb.service.LibraryService;

@Service
public class BookCopyService {
	@Autowired
	bookCopyRepository bookCopyRepository;

	@Autowired
	BookService bookService;

	@Autowired
	LentBookService lentBookService;

	@Autowired
	LibraryService libraryService;

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public BookCopy addBookCopy(BookCopy bookCopy) {
		return bookCopyRepository.save(bookCopy);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public Long getBookCopyCount(String isbn, Boolean available, Long libraryId) {
		return bookCopyRepository.findBookCopyCount(isbn, libraryId, available);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	@Transactional
	public void deleteBookCopy(Long id) {
		BookCopy bookCopy = bookCopyRepository.findOneByBookId(id);
		lentBookService.deleteByBookCopy(bookCopy);
		bookCopyRepository.deleteById(id);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public void deleteAllBookCopies(Book book) {
		List<BookCopy> bookCopies = bookCopyRepository.findAllByBook(book);
		for (BookCopy bookCopy : bookCopies) {
			lentBookService.deleteByBookCopy(bookCopy);
			bookCopyRepository.delete(bookCopy);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void deleteAllLibraryBooks(Library library) {
		List<BookCopy> bookCopies = bookCopyRepository.findAllByLibrary(library);
		for (BookCopy bookCopy : bookCopies) {
			lentBookService.deleteByBookCopy(bookCopy);
			bookCopyRepository.delete(bookCopy);
		}
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public BookCopy updateBookCopy(BookCopy updatedBookCopy) {
		BookCopy bookCopy = bookCopyRepository.findOneByBookId(updatedBookCopy.getBookId());
		bookCopy.setAvailable(updatedBookCopy.isAvailable());
		Library library = libraryService.getLibrary(updatedBookCopy.getLibrary().getLibraryId());
		bookCopy.setLibrary(library);
		return bookCopyRepository.save(bookCopy);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public List<BookCopy> getAllBookCopiesByAdmin(String isbn, Integer pageNumber, Integer pageSize, Boolean available,
			Long libraryId) {
		Book book = bookService.getBookByIsbn(isbn);
		Library library = libraryService.getLibrary(libraryId);
		return bookCopyRepository.findAllByBookAndLibraryAndAvailable(PageRequest.of(pageNumber - 1, pageSize), book,
				library, available);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public List<BookCopy> getLibraryBookCopies(Integer pageNumber, Integer pageSize, Long libraryId) {
		Library library = libraryService.getLibrary(libraryId);
		return bookCopyRepository.findAllByLibrary(PageRequest.of(pageNumber - 1, pageSize), library);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
	public Long getLibraryBookCopyCount(Long libraryId) {
		return bookCopyRepository.findLibraryBookCopyCount(libraryId);
	}

	public List<BookCopy> getAllBookCopiesByUser(String isbn, Long libraryId) {
		Book book = bookService.getBookByIsbn(isbn);
		return bookCopyRepository.findAllByBookForUser(book, libraryId);
	}
}
