package com.example.ibdb.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.example.ibdb.controller.exception.FieldException;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookComment;
import com.example.ibdb.domain.User;
import com.example.ibdb.model.NotificationType;
import com.example.ibdb.repository.bookCommentRepository;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.service.UserService;

@Service
public class BookCommentService {

	@Autowired
	bookCommentRepository bookCommentRepository;

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	bookRepository bookRepository;

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public BookComment addBookComment(BookComment bookComment) {
		User user = this.userService.getCurrentUser();
		bookComment.setUser(user);
		bookComment.setTimestamp(LocalDateTime.now());
		if (bookComment.getParentComment() != null) {
			notificationService.addNotification(user, NotificationType.COMMENT_REPLY, bookComment.getBook());
		}
		return bookCommentRepository.save(bookComment);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public BookComment updateBookCommentAsUser(BookComment bookComment) {
		BookComment oldComment = bookCommentRepository.findOneByCommentId(bookComment.getCommentId());
		if (oldComment.getUser() != this.userService.getCurrentUser()) {
			throw new FieldException(TranslationService.translate("error.permission.changeComment", null));
		}
		oldComment.setTimestamp(LocalDateTime.now());
		oldComment.setComment(bookComment.getComment());
		oldComment.setDeleted(bookComment.getDeleted());
		oldComment.setEdited(bookComment.getEdited());
		return bookCommentRepository.save(oldComment);
	}

	@PreAuthorize("hasRole('ROLE_EMPLOYEE') or hasRole('ROLE_ADMIN')")
	public BookComment updateBookCommentAsAdmin(BookComment bookComment) {
		BookComment oldComment = bookCommentRepository.findOneByCommentId(bookComment.getCommentId());
		oldComment.setTimestamp(LocalDateTime.now());
		oldComment.setComment(bookComment.getComment());
		oldComment.setDeleted(bookComment.getDeleted());
		oldComment.setEdited(bookComment.getEdited());
		return bookCommentRepository.save(oldComment);
	}

	public List<BookComment> getBookComments(String isbn) {
		Book book = bookRepository.findOneByIsbn(isbn);
		return bookCommentRepository.findAllByBookAndParentCommentIsNull(book);
	}

	public BookComment getBookComment(Long commentId) {
		return bookCommentRepository.findOneByCommentId(commentId);
	}

	public void deleteUserComments(User user) {
		bookCommentRepository.deleteUserComments(user);
	}

	public void deleteBookComments(Book book) {
		bookCommentRepository.deleteByBook(book);
	}
}
