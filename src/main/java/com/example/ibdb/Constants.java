package com.example.ibdb;

public class Constants {
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String WAIT_LIST_AVAILABILITY_CHECK_INTERVAL = "0 0 5 * * ?";
}
