package com.example.ibdb.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.view.Views;

@Entity
public class CommentRating {
	@Id
	@GeneratedValue
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long id;

	@NotNull
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long rating;

	@ManyToOne(fetch = FetchType.LAZY)
	private BookComment bookComment;
    
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}

	public BookComment getBookComment() {
		return bookComment;
	}

	public void setBookComment(BookComment bookComment) {
		this.bookComment = bookComment;
	}

	public Long getUser() {
		return user.getUserId();
	}

	public void setUser(User user) {
		this.user = user;
	}
}
