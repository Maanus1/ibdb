package com.example.ibdb.domain;

import com.example.ibdb.Constants;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long userId;

	@Column(unique = true)
	@NotEmpty(message = "{error.user.emailNotEmpty}")
	@Email(message = "{error.user.emailFormat}")
	@JsonView(Views.AdminView.class)
	private String email;

	@JsonIgnore
	private String password;

	@NotEmpty(message = "{error.user.aliasNotEmpty}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@Column(unique = true)
	private String alias;

	@NotEmpty(message = "{error.user.firstNameNotEmpty}")
	@JsonView(Views.AdminView.class)
	private String firstName;

	@NotEmpty(message = "{error.user.lastNameNotEmpty}")
	@JsonView(Views.AdminView.class)
	private String lastName;

	@NotNull(message = "{error.user.birthYearNotEmpty}")
	@JsonView(Views.AdminView.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
	private LocalDate birthday;

	@ManyToMany(fetch = FetchType.EAGER)
	@JsonView(Views.AdminView.class)
	private Set<Role> roles;

	@OneToMany(mappedBy = "user")
	private List<Rating> ratings;

	@OneToMany(mappedBy = "user")
	private List<LentBook> lentBooks;

	@OneToMany(mappedBy = "user")
	private List<Notification> notifications;
    
	@OneToMany(mappedBy = "user")
	private List<CommentRating> commentRatings;
	
	@OneToMany(mappedBy = "user")
	private List<BookWaitListItem> bookWaitList;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Library preferredLibrary;

	public User() {
	}

	public User(String email, String password, String firstName, String lastName, LocalDate birthday, String alias,
			List<Role> roles) {
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.alias = alias;
		this.roles = new HashSet<>(roles);
	}

	public User(String email, String password, String firstName, String lastName, LocalDate birthday, String alias) {
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.alias = alias;
	}

	public User(String firstName, String lastName, String email, Long userId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public String getPassword() {
		return password;
	}
    
	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<LentBook> getLentBooks() {
		return lentBooks;
	}

	public void setLentBooks(List<LentBook> lentBooks) {
		this.lentBooks = lentBooks;
	}

	public Library getPreferredLibrary() {
		return preferredLibrary;
	}

	public void setPreferredLibrary(Library preferredLibrary) {
		this.preferredLibrary = preferredLibrary;
	}

	public List<BookWaitListItem> getBookWaitList() {
		return bookWaitList;
	}

	public void setBookWaitList(List<BookWaitListItem> bookWaitList) {
		this.bookWaitList = bookWaitList;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
}
