package com.example.ibdb.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.List;

@Entity
public class Book {
	@Id
	@Column(unique = true, nullable = false)
	@NotEmpty(message = "{error.book.isbnNotEmpty}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String isbn;
	
	@NotEmpty(message = "{error.book.bookTitleNotEmpty}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String bookTitle;
	
	@NotEmpty(message = "{error.book.bookAuthorNotEmpty}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String bookAuthor;
	
	@NotNull(message = "{error.book.publicationYearNotEmpty}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Long publicationYear;
	
	@NotEmpty(message = "{error.book.publisherNotEmpty}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String publisher;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String imageUrl;
	
	@OneToMany(mappedBy = "book")
	private List<Rating> ratings;
	
	@OneToMany(mappedBy = "book")
	private List<BookCopy> bookCopies;
	
	@OneToMany(mappedBy = "book")
	private List<Notification> notifications;
	
	@OneToMany(mappedBy = "book")
	private List<BookWaitListItem> bookWaitList;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@Transient
	private Double averageRating;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@Transient
	private Long bookCopyCount;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@Transient
	boolean available;

	public Book(Book book, Double averageRating, Long bookCopyCount) {
		this.isbn = book.isbn;
		this.bookTitle = book.bookTitle;
		this.bookAuthor = book.bookAuthor;
		this.publicationYear = book.publicationYear;
		this.publisher = book.publisher;
		this.imageUrl = book.imageUrl;
		this.bookCopyCount = bookCopyCount;
		this.averageRating = averageRating;
	}

	public Book(Book book, Double averageRating) {
		this.isbn = book.isbn;
		this.bookTitle = book.bookTitle;
		this.bookAuthor = book.bookAuthor;
		this.publicationYear = book.publicationYear;
		this.publisher = book.publisher;
		this.imageUrl = book.imageUrl;
		this.averageRating = averageRating;
	}

	/*
	 * public Book(Double averageRating, String isbn) { this.isbn = isbn;
	 * this.averageRating = averageRating; }
	 */
	public Book(Book book, Long bookCopyCount) {
		this.isbn = book.isbn;
		this.bookTitle = book.bookTitle;
		this.bookAuthor = book.bookAuthor;
		this.averageRating = book.averageRating;
		this.publicationYear = book.publicationYear;
		this.publisher = book.publisher;
		this.bookCopyCount = bookCopyCount;
	}

	/*
	 * public Book(Book book) { this.isbn = book.isbn; this.bookTitle =
	 * book.bookTitle; this.bookAuthor = book.bookAuthor; this.publicationYear =
	 * book.publicationYear; this.publisher = book.publisher; this.imageUrl =
	 * book.imageUrl; }
	 * 
	 */ public Book() {
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public Long getPublicationYear() {
		return publicationYear;
	}

	public void setPublicationYear(Long publicationYear) {
		this.publicationYear = publicationYear;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getimageUrl() {
		return imageUrl;
	}

	public void setimageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public Double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<BookCopy> getBookCopies() {
		return bookCopies;
	}

	public void setBookCopies(List<BookCopy> bookCopies) {
		this.bookCopies = bookCopies;
	}

	public Long getBookCopyCount() {
		return bookCopyCount;
	}

	public void setBookCopyCount(Long bookCopyCount) {
		this.bookCopyCount = bookCopyCount;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public List<BookWaitListItem> getBookWaitList() {
		return bookWaitList;
	}

	public void setBookWaitList(List<BookWaitListItem> bookWaitList) {
		this.bookWaitList = bookWaitList;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
}
