package com.example.ibdb.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.view.Views;


@Entity
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView({Views.AdminView.class,Views.UserView.class})
    private Long ratingId;
    
    @NotNull(message = "{error.rating.rating}")
    @JsonView({Views.AdminView.class,Views.UserView.class})
    private Long rating;
    
    @JsonView({Views.AdminView.class,Views.UserView.class})
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    
    @JsonView({Views.AdminView.class,Views.UserView.class})
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull(message = "{error.rating.book}")
    private Book book;
    

    public Rating() {
    }
    
    public Rating(Long ratingId, Long rating) {
    	this.ratingId = ratingId;
    	this.rating = rating;
    }
    
    public Long getRatingId() {
        return ratingId;
    }

    public void setRatingId(Long ratingId) {
        this.ratingId = ratingId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

}
