package com.example.ibdb.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.ibdb.model.NotificationType;
import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.view.Views;

@Entity
public class Notification {
	@Id
	@GeneratedValue
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long notificationId;

	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private boolean read = false;

	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private LocalDateTime timestamp;

	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private NotificationType notificationType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private User user;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Book book;
    
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Library library;
	
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@Transient
	private String message;

	public Notification() {
	}

	public Notification(User user, NotificationType notificationType) {
		this.user = user;
		this.notificationType = notificationType;
	}

	public Notification(User user, NotificationType notificationType, Book book) {
		this.user = user;
		this.notificationType = notificationType;
		this.book = book;
	}
    
	public Notification(User user, NotificationType notificationType, Book book, Library library) {
		this.user = user;
		this.notificationType = notificationType;
		this.book = book;
		this.library = library;
	}
	
	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}
	
	
}
