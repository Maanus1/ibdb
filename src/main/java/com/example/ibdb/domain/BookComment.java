package com.example.ibdb.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class BookComment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long commentId;

	@NotEmpty(message = "{error.bookComment.commentNotEmpty}")
	@Size(max = 700, message = "{error.bookComment.commentLength}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private String comment;

	@NotNull(message = "{error.bookComment.deletedNotEmpty}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Boolean deleted = false;

	@NotNull(message = "{error.bookComment.editedNotEmpty}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Boolean edited = false;

	@NotNull(message = "{error.bookComment.timestampNotEmpty}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private LocalDateTime timestamp = LocalDateTime.now();

	@ManyToOne
	private BookComment parentComment;
	
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@OneToMany(mappedBy = "parentComment")
	private List<BookComment> childComments;
    
	@OneToMany(mappedBy = "bookComment")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private List<CommentRating> commentRatings;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private User user;

	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull(message = "{error.bookComment.bookNotNull}")
	private Book book;

	public BookComment() {
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Boolean getEdited() {
		return edited;
	}

	public void setEdited(Boolean edited) {
		this.edited = edited;
	}

	public BookComment getParentComment() {
		return parentComment;
	}

	public void setParentComment(BookComment parentComment) {
		this.parentComment = parentComment;
	}

	public List<BookComment> getChildComments() {
		return childComments;
	}

	public void setChildComments(List<BookComment> childComments) {
		this.childComments = childComments;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
}
