package com.example.ibdb.domain;

import java.util.List;

public class ApiError {
    private List<String> errors;
    public ApiError(List<String> errors, String errorMessage) {
        this.errors = errors;
    }
    
    public ApiError(List<String> errors) {
        this.errors = errors;
    }
    
    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
