package com.example.ibdb.domain;

import java.time.LocalTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.view.Views;

@Entity 
public class Library {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Long libraryId;
	
	@NotEmpty(message = "{error.library.addressNotNull}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private String address;
	
	@NotNull(message = "{error.library.latitudeNotNull}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Double latitude;
	
	@NotNull(message = "{error.library.longitudeNotNull}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Double longitude;
	
	@NotNull(message = "{error.library.openingTimeNotNull}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private LocalTime openingTime;
	
	@NotNull(message = "{error.library.closingTimeNotNull}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private LocalTime closingTime;
	
	@OneToMany(mappedBy = "library")
	private List<BookCopy> bookCopies;
	
	@OneToMany(mappedBy = "library")
	private List<LentBook> lentBooks;
	
	@OneToMany(mappedBy = "library")
	private List<BookWaitListItem> bookWaitList;
	
	@OneToMany(mappedBy = "preferredLibrary")
	private List<User> users;
    
	@OneToMany(mappedBy = "library")
	private List<Notification> notifications;
	
	public Library() {
	}

	public Library(Long libraryId, String address) {
		this.libraryId = libraryId;
		this.address = address;
	}

	public Long getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(Long libraryId) {
		this.libraryId = libraryId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public LocalTime getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(LocalTime openingTime) {
		this.openingTime = openingTime;
	}

	public LocalTime getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(LocalTime closingTime) {
		this.closingTime = closingTime;
	}

	public List<BookCopy> getBookCopies() {
		return bookCopies;
	}
	
	public void setBookCopies(List<BookCopy> bookCopies) {
		this.bookCopies = bookCopies;
	}

	public List<LentBook> getLentBooks() {
		return lentBooks;
	}
	
	public void setLentBooks(List<LentBook> lentBooks) {
		this.lentBooks = lentBooks;
	}

	public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<BookWaitListItem> getBookWaitList() {
		return bookWaitList;
	}

	public void setBookWaitList(List<BookWaitListItem> bookWaitList) {
		this.bookWaitList = bookWaitList;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
}
