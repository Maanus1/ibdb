package com.example.ibdb.domain;

import com.example.ibdb.model.RoleName;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Role {

    @Id
    @GeneratedValue
    @JsonView({Views.AdminView.class,Views.UserView.class})
    private Long roleId;

    @Enumerated(EnumType.STRING)
    @NotNull
    @JsonView({Views.AdminView.class,Views.UserView.class})
    private RoleName name;

    public Role(RoleName name) {
        this.name = name;
    }

    public Role() {
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }
}
