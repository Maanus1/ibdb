package com.example.ibdb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class BookWaitListItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Long waitListId;
	
	@ManyToOne
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Book book;
	
	@ManyToOne
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private User user;
	
	@ManyToOne(optional = true)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Library library;
	
	@Transient
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Long waitListItemCount;
	
	public BookWaitListItem() {
	}

	public BookWaitListItem(Library library, User user, Book book) {
		this.library = library;
		this.user = user;
		this.book = book;
	}
	
	public BookWaitListItem(User user, Book book) {
		this.user = user;
		this.book = book;
	}
	
	public BookWaitListItem(Book book,Library library, Long waitListCount) {
		this.book = book;
		this.library = library;
		this.waitListItemCount=waitListCount;
	}
	
	public BookWaitListItem(Book book,Library library) {
		this.book = book;
		this.library = library;
	}
	
	public Long getWaitListId() {
		return waitListId;
	}

	public void setWaitListId(Long waitListId) {
		this.waitListId = waitListId;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

	public Long getWaitListItemCount() {
		return waitListItemCount;
	}

	public void setWaitListItemCount(Long waitListItemCount) {
		this.waitListItemCount = waitListItemCount;
	}
}
