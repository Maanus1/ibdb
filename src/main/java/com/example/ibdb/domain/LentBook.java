package com.example.ibdb.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.example.ibdb.Constants;
import com.example.ibdb.model.BookLendingStatus;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class LentBook {
	@Id
	@GeneratedValue
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@NotNull(message = "{error.lentBook.lendingStatus}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private BookLendingStatus lendingStatus = BookLendingStatus.BOOK_RESERVED;
	
	@NotNull(message = "{error.lentBook.fromDate}")
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate fromDate;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@NotNull(message = "{error.lentBook.toDate}")
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate toDate;
	
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private LocalDateTime timestamp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@NotNull(message = "{error.lentBook.bookCopy}")
	private BookCopy bookCopy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	private User user;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JsonView({Views.AdminView.class,Views.UserView.class})
	@NotNull(message = "{error.lentBook.library}")
	private Library library;

	public LentBook() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BookLendingStatus getBookLendingStatus() {
		return lendingStatus;
	}

	public void setBookLendingStatus(BookLendingStatus lendingStatus) {
		this.lendingStatus = lendingStatus;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public BookCopy getBookCopy() {
		return bookCopy;
	}

	public void setBookCopy(BookCopy bookCopy) {
		this.bookCopy = bookCopy;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}
}
