package com.example.ibdb.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.view.Views;

@Entity
public class BookCopy {
	@Id
	@GeneratedValue
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private Long bookId;

	@NotNull
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private boolean available = true;

	@NotNull(message = "{error.bookCopy.boughtTimeNotNull}")
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	private LocalDateTime bought = LocalDateTime.now();

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@NotNull(message = "{error.bookCopy.bookNotNull}")
	private Book book;

	@OneToMany(mappedBy = "bookCopy")
	private List<LentBook> lentBooks;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView({ Views.AdminView.class, Views.UserView.class })
	@NotNull(message = "{error.bookCopy.libraryNotNull}")
	private Library library;

	public BookCopy() {
	}

	public BookCopy(Long bookId) {
		this.bookId = bookId;
	}

	public BookCopy(Book book, Library library) {
		this.book = book;
		this.library = library;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<LentBook> getLentBooks() {
		return lentBooks;
	}

	public void setLentBooks(List<LentBook> lentBooks) {
		this.lentBooks = lentBooks;
	}

	public LocalDateTime getBought() {
		return bought;
	}

	public void setBought(LocalDateTime bought) {
		this.bought = bought;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

	@Override
	public String toString() {
		return "Book ID: " + this.getBookId() + ", Available: " + this.isAvailable();
	}
}
