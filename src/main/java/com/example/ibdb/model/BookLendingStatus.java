package com.example.ibdb.model;

public enum BookLendingStatus {
	BOOK_RESERVED,
	ACTIVE,
	BOOK_RETURNED,
	CANCELLED;
	
	public static BookLendingStatus fromValue(String value) {
		if (value == "" || value == null) {
			return null;
		}
		return BookLendingStatus.valueOf(value);
	}
}
