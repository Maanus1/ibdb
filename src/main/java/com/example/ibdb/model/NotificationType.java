package com.example.ibdb.model;

public enum NotificationType {
	LENDING_ACTIVATED("notification.lending.lendingActivated"),
	USER_INFORMATION("notification.user.userInformationChanged"),
	BOOK_AVAILABLE("notification.lending.bookAvailable"),
	BOOK_RETURNED("notification.lending.bookReturned"),
	RESERVATION_CANCELLED("notification.lending.reservationCancelled"),
	COMMENT_REPLY("notification.bookComment.commentRepliedTo");
	
	private final String message;
	
	private NotificationType(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
