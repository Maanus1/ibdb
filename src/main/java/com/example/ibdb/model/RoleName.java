package com.example.ibdb.model;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
    ROLE_USER
}
