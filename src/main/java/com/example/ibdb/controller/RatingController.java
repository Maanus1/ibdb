package com.example.ibdb.controller;

import com.example.ibdb.domain.Rating;
import com.example.ibdb.service.RatingService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.repository.bookRepository;
import com.example.ibdb.repository.ratingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

@RestController
public class RatingController {
	@Autowired
	RatingService ratingService;

	@Autowired
	ratingRepository ratingRepository;

	@Autowired
	bookRepository bookRepository;
    
	@JsonView(Views.UserView.class)
	@GetMapping("ratings")
	public Map<String, Object> getRatings(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhase,
			@RequestParam(value = "startAge") Long startAge, @RequestParam(value = "endAge") Long endAge,
			@RequestParam(value = "minimumRating", required = false) Double minimumRating,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return ratingService.getRatings(pageNumber, pageSize, searchPhase, startAge, endAge, minimumRating, 
				libraryId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("ratings/user-ratings")
	public List<Rating> getCurrentUserRatings(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return ratingService.getCurrentUserRatings(pageNumber, pageSize, searchPhrase);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("ratings/user-ratings-count")
	public Long getCurrentUserRatingCount(@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return ratingService.getCurrentUserRatingCount(searchPhrase);
	}
    
	@JsonView(Views.UserView.class)
	@PostMapping("ratings")
	public Rating addRating(@Valid @RequestBody Rating rating) {
		return ratingService.addRating(rating);
	}
    
	@JsonView(Views.UserView.class)
	@PutMapping("ratings/{ratingId}")
	public void updateRating(@Valid @RequestBody Rating rating) {
		ratingService.updateRating(rating);
	}
	
	@DeleteMapping("ratings/{ratingId}")
	public void deleteRating(@PathVariable(value = "ratingId") Long ratingId) {
		ratingService.deleteRating(ratingId);
	}
}
