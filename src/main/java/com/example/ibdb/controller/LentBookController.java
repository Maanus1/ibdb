package com.example.ibdb.controller;

import com.example.ibdb.Constants;
import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.LentBook;
import com.example.ibdb.domain.User;
import com.example.ibdb.service.LentBookService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

@RestController
public class LentBookController {
	@Autowired
	LentBookService lentBookService;
    
	@JsonView(Views.UserView.class)
	@GetMapping("book-copies/{bookId}/lent-books")
	public List<LentBook> getBookCopyLendingData(@PathVariable(value = "bookId") Long bookId) {
		return lentBookService.getBookCopyLendingData(bookId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("book-copies/{bookId}/lent-books/reserved-days")
	public List<LocalDate> getBookReservedDays(@PathVariable(value = "bookId") Long bookId) {
		return lentBookService.getBookReservedDays(bookId);
	}
    
	@JsonView(Views.UserView.class)
	@PostMapping("lent-books")
	public LentBook addLentBook(@Valid @RequestBody LentBook lentBook) {
		return lentBookService.addLentBook(lentBook);
	}
    
	@JsonView(Views.AdminView.class)
	@PutMapping("lent-books/{bookId}/update-lending-status")
	public LentBook updateLentBookStatus(@PathVariable(value = "bookId") Long bookId) {
		return lentBookService.updateLentBookStatus(bookId);
	}
    
	@JsonView(Views.UserView.class)
	@PutMapping("lent-books/{bookId}/cancel-lending-by-admin")
	public LentBook cancelBookLending(@PathVariable(value = "bookId") Long bookId) {
		return lentBookService.cancelBookLending(bookId);
	}
    
	@JsonView(Views.AdminView.class)
	@PutMapping("lent-books/{bookId}/cancel-lending-by-user")
	public LentBook cancelBookLendingByAdmin(@PathVariable(value = "bookId") Long bookId) {
		return lentBookService.cancelBookLendingByAdmin(bookId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("lent-books/user")
	public List<LentBook> getCurrentUserLentBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getCurrentUserLentBooks(pageNumber, pageSize, searchPhrase, lendingStatus, fromDate,
				toDate, libraryId);
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("lent-books/user-unique-books")
	public List<Book> getCurrentUserUniqueLentBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getCurrentUserUniqueLentBooks(pageNumber, pageSize, searchPhrase, lendingStatus, fromDate,
				toDate, libraryId);
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("lent-books/user-count")
	public Long getCurrentUserLentBookCount(@RequestParam(value = "searchPhrase", required = false) String searchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getCurrentUserLentBookCount(searchPhrase, lendingStatus, fromDate, toDate, libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("lent-books")
	public List<LentBook> getLentBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "bookSearchPhrase", required = false) String bookSearchPhrase,
			@RequestParam(value = "userSearchPhrase", required = false) String userSearchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getLentBooks(pageNumber, pageSize, bookSearchPhrase, userSearchPhrase, lendingStatus,
				fromDate, toDate, libraryId);
	}
	
	@JsonView(Views.AdminView.class)
	@GetMapping("lent-books/unique-users")
	public List<User> getUniqueLentBookUsers(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "bookSearchPhrase", required = false) String bookSearchPhrase,
			@RequestParam(value = "userSearchPhrase", required = false) String userSearchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getUniqueLentBookUsers(pageNumber, pageSize, bookSearchPhrase, userSearchPhrase, lendingStatus,
				fromDate, toDate, libraryId);
	}
	
	@JsonView(Views.AdminView.class)
	@GetMapping("lent-books/unique-books")
	public List<Book> getUniqueLentBookBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "bookSearchPhrase", required = false) String bookSearchPhrase,
			@RequestParam(value = "userSearchPhrase", required = false) String userSearchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getUniqueLentBookBooks(pageNumber, pageSize, bookSearchPhrase, userSearchPhrase, lendingStatus,
				fromDate, toDate, libraryId);
	}
	
	@JsonView(Views.AdminView.class)
	@GetMapping("lent-books/count")
	public Long getLentBookCount(@RequestParam(value = "bookSearchPhrase", required = false) String bookSearchPhrase,
			@RequestParam(value = "userSearchPhrase", required = false) String userSearchPhrase,
			@RequestParam(value = "lendingStatus", required = false) String lendingStatus,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_FORMAT) LocalDate toDate,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return lentBookService.getLentBookCount(bookSearchPhrase, userSearchPhrase, lendingStatus, fromDate, toDate,
				libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("lent-books/lending-statuses")
	public List<String> getAllLendingStatuses() {
		return lentBookService.getAllLendingStatuses();
	}
}
