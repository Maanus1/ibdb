package com.example.ibdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.BookWaitListItem;
import com.example.ibdb.service.BookWaitListItemService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class BookWaitListItemController {
	@Autowired
	BookWaitListItemService bookWaitListItemService;

	@JsonView(Views.UserView.class)
	@PostMapping("book-wait-list")
	public BookWaitListItem addToWaitList(@RequestBody BookWaitListItem waitListItem) {
		return bookWaitListItemService.addToWaitList(waitListItem);
	}

	@DeleteMapping("book-wait-list/{waitListId}")
	public void deleteBookWaitListItem(@PathVariable("waitListId") Long waitListId) {
		bookWaitListItemService.deleteBookWaitListItem(waitListId);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/book-wait-list/user-item")
	public BookWaitListItem getWaitListItem(@RequestParam(value = "libraryId", required = false) Long libraryId,
			@PathVariable("isbn") String isbn) {
		return bookWaitListItemService.getWaitListItem(libraryId, isbn);
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("book-wait-list")
	public List<BookWaitListItem> getWaitList(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "libraryId", required = false) Long libraryId,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookWaitListItemService.getWaitList(pageNumber, pageSize, libraryId, searchPhrase);
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("book-wait-list/count")
	public Long getWaitListCount(@RequestParam(value = "libraryId", required = false) Long libraryId,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookWaitListItemService.getWaitListCount(libraryId, searchPhrase);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("book-wait-list/current-user")
	public List<BookWaitListItem> getUserWaitList(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "libraryId", required = false) Long libraryId,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookWaitListItemService.getUserWaitList(pageNumber, pageSize, libraryId, searchPhrase);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("book-wait-list/current-user-unique-books")
	public List<Book> getUserWaitListUniqueBooks(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "libraryId", required = false) Long libraryId,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookWaitListItemService.getUserWaitListUniqueBooks(pageNumber, pageSize, libraryId, searchPhrase);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("book-wait-list/current-user-count")
	public Long getUserWaitListCount(@RequestParam(value = "libraryId", required = false) Long libraryId,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookWaitListItemService.getUserWaitListCount(libraryId, searchPhrase);
	}
}
