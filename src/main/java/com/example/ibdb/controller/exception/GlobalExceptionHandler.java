package com.example.ibdb.controller.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import com.example.ibdb.domain.ApiError;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler({ MethodArgumentNotValidException.class, FieldException.class,
			UsernameNotFoundException.class/* ,Exception.class */ })
	public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {
		HttpHeaders headers = new HttpHeaders();
		if (ex instanceof MethodArgumentNotValidException) {
			HttpStatus status = HttpStatus.BAD_REQUEST;
			MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
			return handleMethodArgumentNotValidException(exception, headers, status, request);
		} else if (ex instanceof FieldException) {
			HttpStatus status = HttpStatus.FORBIDDEN;
			FieldException exception = (FieldException) ex;
			return handleFieldException(exception, headers, status, request);
		} else if (ex instanceof UsernameNotFoundException) {
			HttpStatus status = HttpStatus.FOUND;
			UsernameNotFoundException exception = (UsernameNotFoundException) ex;
			return handleFieldException(exception, headers, status, request);
		} else {
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return handleExceptionInternal(ex, null, headers, status, request);
		}
	}

	protected ResponseEntity<ApiError> handleFieldException(Exception ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		return handleExceptionInternal(ex, new ApiError(Collections.singletonList(ex.getMessage())), headers, status,
				request);
	}

	protected ResponseEntity<ApiError> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorList = new ArrayList<>();
		for (FieldError f : ex.getBindingResult().getFieldErrors()) {
			errorList.add(f.getDefaultMessage());
		}
		return handleExceptionInternal(ex, new ApiError(errorList), headers, status, request);
	}

	protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, ApiError body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}

		return new ResponseEntity<>(body, headers, status);
	}
}
