package com.example.ibdb.controller.exception;

public class FieldException extends RuntimeException { 

	private static final long serialVersionUID = 1L;

	public FieldException(String errorMessage) {
        super(errorMessage);
    }
}