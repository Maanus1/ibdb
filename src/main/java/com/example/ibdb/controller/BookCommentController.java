package com.example.ibdb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ibdb.domain.BookComment;
import com.example.ibdb.service.BookCommentService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class BookCommentController {
    
	@Autowired
	BookCommentService bookCommentService;
	
	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/book-comments")
	public List<BookComment> getBookComments(@PathVariable(value = "isbn") String isbn) {
		return bookCommentService.getBookComments(isbn);
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("book-comments/{id}")
	public BookComment getBookComment(@PathVariable(value = "id") Long id) {
		return bookCommentService.getBookComment(id);
	}
	
	@JsonView(Views.UserView.class)
	@PostMapping("book-comments")
	public BookComment addBookComment(@Valid @RequestBody BookComment bookComment) {
		return bookCommentService.addBookComment(bookComment);
	}
	
	@JsonView(Views.UserView.class)
	@PutMapping("book-comments/{id}/by-user")
	public BookComment updateBookCommentAsUser(@RequestBody BookComment bookComment) {
		return bookCommentService.updateBookCommentAsUser(bookComment);
	}
	
	@JsonView(Views.UserView.class)
	@PutMapping("book-comments/{id}/by-admin")
	public BookComment updateBookCommentAsAdmin(@RequestBody BookComment bookComment) {
		return bookCommentService.updateBookCommentAsAdmin(bookComment);
	}
}
