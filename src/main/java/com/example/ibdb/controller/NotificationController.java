package com.example.ibdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ibdb.domain.Notification;
import com.example.ibdb.service.NotificationService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class NotificationController {
	
	@Autowired
	NotificationService notificationService;
	
	@JsonView(Views.UserView.class)
	@GetMapping("notifications")
	public List<Notification> getNotifications(@RequestParam(value = "pageNumber") Integer pageNumber,
			   @RequestParam(value = "pageSize") Integer pageSize) {
		return notificationService.getUserNotifications(pageNumber,pageSize);
	}
	
	@JsonView(Views.UserView.class)
	@PutMapping("notifications/mark-as-seen")
	public void markNotificationsAsSeen() {
		notificationService.markNotificationsAsSeen();
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("notifications/unread-count")
	public Long getUnreadNotificationCount() {
		return notificationService.getUnreadNotificationCount();
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("notifications/count")
	public Long getNotificationCount() {
		return notificationService.getNotificationCount();
	}
}
