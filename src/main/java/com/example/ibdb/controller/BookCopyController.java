package com.example.ibdb.controller;

import com.example.ibdb.domain.BookCopy;
import com.example.ibdb.domain.Library;
import com.example.ibdb.service.BookCopyService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookCopyController {
	@Autowired
	BookCopyService bookCopyService;
 
	@DeleteMapping("book-copies/{id}")
	public void deleteBookCopy(@PathVariable(value = "id") Long id) {
		bookCopyService.deleteBookCopy(id);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("books/{isbn}/book-copies/count")
	public Long getBookCopyCount(@PathVariable(value = "isbn") String isbn,
			@RequestParam(value = "available", required = false) Boolean available,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return bookCopyService.getBookCopyCount(isbn,available, libraryId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/book-copies")
	public List<BookCopy> getAllBookCopies(@PathVariable(value = "isbn") String isbn,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return bookCopyService.getAllBookCopiesByUser(isbn, libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("books/{isbn}/book-copies/by-admin")
	public List<BookCopy> getAllBookCopiesAdmin(@PathVariable(value = "isbn") String isbn,
			@RequestParam(value = "pageNumber") Integer pageNumber, @RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "available", required = false) Boolean available,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return bookCopyService.getAllBookCopiesByAdmin(isbn, pageNumber, pageSize, available, libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("libraries/{libraryId}/book-copies")
	public List<BookCopy> getLibraryBookCopies(@PathVariable(value = "libraryId") Long libraryId,
			@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize) {
		return bookCopyService.getLibraryBookCopies(pageNumber, pageSize, libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("libraries/{libraryId}/book-copies/count")
	public Long getLibraryBookCopyCount(@PathVariable(value = "libraryId") Long libraryId) {
		return bookCopyService.getLibraryBookCopyCount(libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@PostMapping("book-copies")
	public BookCopy addBookCopy(@Valid @RequestBody BookCopy bookCopy) {
		return bookCopyService.addBookCopy(bookCopy);
	}
    
	@JsonView(Views.AdminView.class)
	@PutMapping("book-copies/{id}")
	public BookCopy updateBookCopy(@Valid @RequestBody BookCopy bookCopy) {
		return bookCopyService.updateBookCopy(bookCopy);
	}
}
