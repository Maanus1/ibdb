package com.example.ibdb.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ibdb.domain.CommentRating;
import com.example.ibdb.service.CommentRatingService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class CommentRatingController {
	@Autowired
	CommentRatingService commentRatingService;

	@JsonView(Views.UserView.class)
	@PostMapping("comment-rating")
	public CommentRating addCommentRating(@Valid @RequestBody CommentRating commentRating) {
		return commentRatingService.addCommentRating(commentRating);
	}

	@JsonView(Views.UserView.class)
	@PutMapping("comment-rating/{id}")
	public CommentRating updateCommentRating(@Valid @RequestBody CommentRating commentRating) {
		return commentRatingService.updateCommentRating(commentRating);
	}

	@JsonView(Views.UserView.class)
	@DeleteMapping("comment-rating/{id}")
	public void deleteCommentRating(@PathVariable(value = "id") Long id) {
		commentRatingService.deleteCommentRating(id);
	}
}
