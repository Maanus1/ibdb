package com.example.ibdb.controller;

import com.example.ibdb.domain.Role;
import com.example.ibdb.domain.User;
import com.example.ibdb.service.NotificationService;
import com.example.ibdb.service.UserService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.repository.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
public class UserController {
	@Autowired
	userRepository userRepository;

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	@JsonView(Views.AdminView.class)
	@GetMapping("users")
	public List<User> getUsers(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return userService.getUsers(pageNumber, pageSize, searchPhrase);
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("users/current")
	public User getCurrentUser() {
		return userService.getCurrentUser();
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("users/count")
	public Long getUserCount(@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return userService.findUserCount(searchPhrase);
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("users/roles")
	public List<Role> getRoles() {
		return userService.getRoles();
	}

	@JsonView(Views.UserView.class)
	@PostMapping("users")
	public User createUser(@Valid @RequestBody User user) {
		return userService.createUser(user);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("users/is-email-taken")
	public Boolean isEmailTaken(@RequestParam(value = "email") String email,
			@RequestParam(value = "userId", required = false) Long userId) {
		return userService.isEmailTaken(email,userId);
	}

	@JsonView(Views.UserView.class)
	@GetMapping("users/is-alias-taken")
	public Boolean isAliasTaken(@RequestParam(value = "alias") String alias,
			@RequestParam(value = "userId", required = false) Long userId) {
		return userService.isAliasTaken(alias, userId);
	}

	@JsonView(Views.UserView.class)
	@PostMapping("users/change-password")
	public User changePassword(@RequestParam(value = "currentPassword") String currentPassword,
			@RequestParam(value = "newPassword") String newPassword) {
		return userService.changePassword(currentPassword, newPassword);
	}

	@JsonView(Views.UserView.class)
	@PutMapping("users/{userId}/by-user")
	public User updateUser(@Valid @RequestBody User user) {
		return userService.updateUser(user);
	}

	@JsonView(Views.AdminView.class)
	@PutMapping("users/{userId}/by-admin")
	public User updateUserByAdmin(@Valid @RequestBody User user) {
		return userService.updateUserByAdmin(user);
	}

	@JsonView(Views.AdminView.class)
	@GetMapping("users/{userId}")
	public User getUser(@PathVariable(value = "userId") Long userId) {
		return userService.getUserByUserId(userId);
	}

	@DeleteMapping("users/{userId}")
	public void deleteUser(@PathVariable(value = "userId") Long userId) {
		userService.deleteUser(userId);
	}
}