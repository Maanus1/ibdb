package com.example.ibdb.controller;

import com.example.ibdb.domain.Book;
import com.example.ibdb.domain.Rating;
import com.example.ibdb.service.BookService;
import com.example.ibdb.service.RatingService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import com.example.ibdb.repository.bookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.Map;

@RestController
public class BookController {
	@Autowired
	bookRepository bookRepository;

	@Autowired
	BookService bookService;
	
	@Autowired
	RatingService ratingService;
    
	@JsonView(Views.AdminView.class)
	@GetMapping("books")
	public List<Book> getBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "searchPhrase", required = false) String searchPhrase) {
		return bookService.getBooks(pageNumber, pageSize, searchPhrase);
	}
	
	@JsonView(Views.AdminView.class)
	@GetMapping("books/count")
	public Long getBookCount(@RequestParam(value = "searchPhrase", required = false) String searchPhase,
			@RequestParam(value = "startAge", required = false) Long startAge,
			@RequestParam(value = "endAge", required = false) Long endAge,
			@RequestParam(value = "minimumRating", required = false) Double minimumRating) {
		return bookService.findBookCount(searchPhase, startAge, endAge, minimumRating);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("books/{isbn}/is-isbn-taken")
	public Boolean isIsbnTaken(@PathVariable(value = "isbn") String isbn) {
		return bookService.isIsbnTaken(isbn);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("books/{isbn}")
	public Book getBook(@PathVariable(value = "isbn") String isbn) {
		return bookService.getBookByIsbn(isbn);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("books/recommended")
	public Map<String, Object> getRecommendedBooks(@RequestParam(value = "pageNumber") Integer pageNumber,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "startAge") Long startAge, 
			@RequestParam(value = "endAge") Long endAge,
			@RequestParam(value = "filterPhrase", required = false) String filterPhrase,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return bookService.getRecommendedBooks(pageNumber,pageSize,startAge,endAge,filterPhrase,libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@PostMapping("books")
	public Book addBook(@Valid @RequestBody Book book) {
		return bookService.addBook(book);
	}
  
	@DeleteMapping("books/{isbn}")
	public void deleteBook(@PathVariable(value = "isbn") String isbn) {
		bookService.deleteBook(isbn);
	}
    
	@JsonView(Views.AdminView.class)
	@PutMapping("books/{isbn}")
	public Book updateBook(@Valid @RequestBody Book book) {
		return bookService.updateBook(book);
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("books/top")
	public List<Book> getTopRatings(@RequestParam(value = "pageSize") Integer topSize,
			@RequestParam(value = "startAge") Long startAge, @RequestParam(value = "endAge") Long endAge,
			@RequestParam(value = "libraryId", required = false) Long libraryId) {
		return bookService.getTopRatedBooks(topSize, startAge, endAge, libraryId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/with-rating")
	public Book getBookWithRating(@PathVariable(value = "isbn") String isbn,
				@RequestParam(value= "libraryId", required = false) Long libraryId) {
		return bookService.getBookWithRating(isbn,libraryId);
	}
    
	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/user-rating")
	public Rating getCurrentUserRating(@PathVariable(value = "isbn") String isbn) {
		return ratingService.getCurrentUserRating(isbn);
	}
	
	@JsonView(Views.UserView.class)
	@GetMapping("books/{isbn}/rating")
	public Double getRating(@PathVariable(value = "isbn") String isbn, @RequestParam(value = "startAge") Long startAge,
			@RequestParam(value = "endAge") Long endAge) {
		return ratingService.getRating(isbn, startAge, endAge);
	}
}
