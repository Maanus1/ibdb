package com.example.ibdb.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ibdb.domain.Library;
import com.example.ibdb.service.LibraryService;
import com.example.ibdb.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class LibraryController {

	@Autowired
	LibraryService libraryService;
    
	@JsonView(Views.AdminView.class)
	@GetMapping("libraries")
	public List<Library> getAllLibrariesWithPagination(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) {
		if(pageNumber != null && pageSize != null) {
			return libraryService.getAllLibrariesWithPagination(pageNumber, pageSize);	
		}
		else {
			return libraryService.getAllLibraries();
		}
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("libraries/{libraryId}")
	public Library getLibrary(@PathVariable(value = "libraryId") Long libraryId) {
		return libraryService.getLibrary(libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@PostMapping("libraries")
	public Library addLibrary(@Valid @RequestBody Library library) {
		return libraryService.addLibrary(library);
	}
    
	@JsonView(Views.AdminView.class)
	@PutMapping("libraries/{libraryId}")
	public Library updateLibrary(@Valid @RequestBody Library library) {
		return libraryService.updateLibrary(library);
	}

	@DeleteMapping("libraries/{libraryId}")
	public void deleteLibrary(@PathVariable("libraryId") Long libraryId) {
		libraryService.deleteLibrary(libraryId);
	}
    
	@JsonView(Views.AdminView.class)
	@GetMapping("libraries/count")
	public Long getLibraryCount() {
		return libraryService.getLibrarycount();
	}
}
